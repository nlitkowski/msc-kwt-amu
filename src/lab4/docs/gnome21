Drawing Area - Lines
This program contains a single class, MyArea, which is a subclass of Gtk::DrawingArea and contains an on_draw() member function. This function is called whenever the image in the drawing area needs to be redrawn. It is passed a Cairo::RefPtr pointer to a Cairo::Context that we use for the drawing. The actual drawing code sets the color we want to use for drawing by using set_source_rgb() which takes arguments defining the Red, Green, and Blue components of the desired color (valid values are between 0 and 1). After setting the color, we created a new path using the functions move_to() and line_to(), and then stroked this path with stroke().
Drawing with relative coordinates
In the example above we drew everything using absolute coordinates. You can also draw using relative coordinates. For a straight line, this is done with the function Cairo::Context::rel_line_to().
Line styles
In addition to drawing basic straight lines, there are a number of things that you can customize about a line. You've already seen examples of setting a line's color and width, but there are others as well.
If you've drawn a series of lines that form a path, you may want them to join together in a certain way. Cairo offers three different ways to join lines together: Miter, Bevel, and Round. These are show below:
Different join types in Cairo
The line join style is set using the function Cairo::Context::set_line_join().
Line ends can have different styles as well. The default style is for the line to start and stop exactly at the destination points of the line. This is called a Butt cap. The other options are Round (uses a round ending, with the center of the circle at the end point) or Square (uses a squared ending, with the center of the square at the end point). This setting is set using the function Cairo::Context::set_line_cap().
There are other things you can customize as well, including creating dashed lines and other things. For more information, see the Cairo API documentation.
Drawing thin lines
If you try to draw one pixel wide lines, you may notice that the line sometimes comes up blurred and wider than it ought to be. This happens because Cairo will try to draw from the selected position, to both sides (half to each), so if you're positioned right on the intersection of the pixels, and want a one pixel wide line, Cairo will try to use half of each adjacent pixel, which isn't possible (a pixel is the smallest unit possible). This happens when the width of the line is an odd number of pixels (not just one pixel).
The trick is to position in the middle of the pixel where you want the line to be drawn, and thus guaranteeing you get the desired results. See Cairo FAQ.
Drawing Area - Thin Lines
Drawing Curved Lines
In addition to drawing straight lines Cairo allows you to easily draw curved lines (technically a cubic Bézier spline) using the Cairo::Context::curve_to() and Cairo::Context::rel_curve_to() functions. These functions take coordinates for a destination point as well as coordinates for two 'control' points. This is best explained using an example, so let's dive in.
This simple application draws a curve with Cairo and displays the control points for each end of the curve.
The only difference between this example and the straight line example is in the on_draw() function, but there are a few new concepts and functions introduced here, so let's examine them briefly.
We make a call to Cairo::Context::scale(), passing in the width and height of the drawing area. This scales the user-space coordinate system such that the width and height of the widget are both equal to 1.0 'units'. There's no particular reason to scale the coordinate system in this case, but sometimes it can make drawing operations easier.
The call to Cairo::Context::curve_to() should be fairly self-explanatory. The first pair of coordinates define the control point for the beginning of the curve. The second set of coordinates define the control point for the end of the curve, and the last set of coordinates define the destination point. To make the concept of control points a bit easier to visualize, a line has been drawn from each control point to the end-point on the curve that it is associated with. Note that these control point lines are both translucent. This is achieved with a variant of set_source_rgb() called set_source_rgba(). This function takes a fourth argument specifying the alpha value of the color (valid values are between 0 and 1).
Drawing Arcs and Circles
With Cairo, the same function is used to draw arcs, circles, or ellipses: Cairo::Context::arc(). This function takes five arguments. The first two are the coordinates of the center point of the arc, the third argument is the radius of the arc, and the final two arguments define the start and end angle of the arc. All angles are defined in radians, so drawing a circle is the same as drawing an arc from 0 to 2 * M_PI radians. An angle of 0 is in the direction of the positive X axis (in user-space). An angle of M_PI/2 radians (90 degrees) is in the direction of the positive Y axis (in user-space). Angles increase in the direction from the positive X axis toward the positive Y axis. So with the default transformation matrix, angles increase in a clockwise direction. (Remember that the positive Y axis points downwards.)
context-gt;save(); context-gt;translate(x, y); context-gt;scale(width / 2.0, height / 2.0); context-gt;arc(0.0, 0.0, 1.0, 0.0, 2 * M_PI); context-gt;restore();
To draw an ellipse, you can scale the current transformation matrix by different amounts in the X and Y directions. For example, to draw an ellipse with center at x, y and size width, height:
Here's an example of a simple program that draws an arc, a circle and an ellipse into a drawing area.
Drawing Area - Arcs
There are a couple of things to note about this example code. Again, the only real difference between this example and the previous ones is the on_draw() function, so we'll limit our focus to that function. In addition, the first part of the function is nearly identical to the previous examples, so we'll skip that portion.
Note that in this case, we've expressed nearly everything in terms of the height and width of the window, including the width of the lines. Because of this, when you resize the window, everything scales with the window. Also note that there are three drawing sections in the function and each is wrapped with a save()/restore() pair so that we're back at a known state after each drawing.
The section for drawing an arc introduces one new function, close_path(). This function will in effect draw a straight line from the current point back to the first point in the path. There is a significant difference between calling close_path() and manually drawing a line back to the starting point, however. If you use close_path(), the lines will be nicely joined together. If you use line_to() instead, the lines will end at the same point, but Cairo won't do any special joining.
Drawing counter-clockwise
The function Cairo::Context::arc_negative() is exactly the same as Cairo::Context::arc() but the angles go the opposite direction.
Drawing Text
Drawing Text with Pango
Text is drawn via Pango Layouts. The easiest way to create a Pango::Layout is to use Gtk::Widget::create_pango_layout(). Once created, the layout can be manipulated in various ways, including changing the text, font, etc. Finally, the layout can be rendered using the Pango::Layout::show_in_cairo_context() method.
Here is an example of a program that draws some text, some of it upside-down. The Printing chapter contains another example of drawing text.
Drawing Area - Text
Drawing Images
There is a method for drawing from a Gdk::Pixbuf to a Cairo::Context. A Gdk::Pixbuf buffer is a useful wrapper around a collection of pixels, which can be read from files, and manipulated in various ways.
Probably the most common way of creating Gdk::Pixbufs is to use Gdk::Pixbuf::create_from_file(), which can read an image file, such as a png file into a pixbuf ready for rendering.
The Gdk::Pixbuf can be rendered by setting it as the source pattern of the Cairo context with Gdk::Cairo::set_source_pixbuf(). Then draw the image with either Cairo::Context::paint() (to draw the whole image), or Cairo::Context::rectangle() and Cairo::Context::fill() (to fill the specified rectangle). set_source_pixbuf() is not a member of Cairo::Context. It takes a Cairo::Context as its first parameter.
Here is a small bit of code to tie it all together: (Note that usually you wouldn't load the image every time in the draw signal handler! It's just shown here to keep it all together.)
bool MyArea::on_draw(const Cairo::RefPtrlt;Cairo::Contextgt;amp; cr) { Glib::RefPtrlt;Gdk::Pixbufgt; image = Gdk::Pixbuf::create_from_file("myimage.png"); // Draw the image at 110, 90, except for the outermost 10 pixels. Gdk::Cairo::set_source_pixbuf(cr, image, 100, 80); cr-gt;rectangle(110, 90, image-gt;get_width()-20, image-gt;get_height()-20); cr-gt;fill(); return true; }
Here is an example of a simple program that draws an image.
Drawing Area - Image
Example Application: Creating a Clock with Cairo
Now that we've covered the basics of drawing with Cairo, let's try to put it all together and create a simple application that actually does something. The following example uses Cairo to create a custom Clock widget. The clock has a second hand, a minute hand, and an hour hand, and updates itself every second.
As before, almost all of the interesting stuff is done in the draw signal handler on_draw(). Before we dig into the draw signal handler, notice that the constructor for the Clock widget connects a handler function on_timeout() to a timer with a timeout period of 1000 milliseconds (1 second). This means that on_timeout() will get called once per second. The sole responsibility of this function is to invalidate the window so that gtkmm will be forced to redraw it.
Now let's take a look at the code that performs the actual drawing. The first section of on_draw() should be pretty familiar by now. This example again scales the coordinate system to be a unit square so that it's easier to draw the clock as a percentage of window size so that it will automatically scale when the window size is adjusted. Furthermore, the coordinate system is scaled over and down so that the (0, 0) coordinate is in the very center of the window.
The function Cairo::Context::paint() is used here to set the background color of the window. This function takes no arguments and fills the current surface (or the clipped portion of the surface) with the source color currently active. After setting the background color of the window, we draw a circle for the clock outline, fill it with white, and then stroke the outline in black. Notice that both of these actions use the _preserve variant to preserve the current path, and then this same path is clipped to make sure that our next lines don't go outside the outline of the clock.
After drawing the outline, we go around the clock and draw ticks for every hour, with a larger tick at 12, 3, 6, and 9. Now we're finally ready to implement the time-keeping functionality of the clock, which simply involves getting the current values for hours, minutes and seconds, and drawing the hands at the correct angles.
Gtk::Widget has several methods and signals which are prefixed with "drag_". These are used for Drag and Drop.
Sources and Destinations
Things are dragged from sources to be dropped on destinations. Each source and destination has infomation about the data formats that it can send or receive, provided by Gtk::TargetEntry items. A drop destination will only accept a dragged item if they both share a compatible Gtk::TargetEntry item. Appropriate signals will then be emitted, telling the signal handlers which Gtk::TargetEntry was used.
target: A name, such as "STRING"
info: An identifier which will be sent to your signals to tell you which TargetEntry was used.
flags: Used only for drag and drop, this specifies whether the data may be dragged to other widgets and applications, or only to the same ones.
Gtk::TargetEntry objects contain this information:
Widgets can be identified as sources or destinations using these Gtk::Widget methods:
void drag_source_set(const std::vectorlt;Gtk::TargetEntrygt;amp; targets, Gdk::ModifierType start_button_mask, Gdk::DragAction actions);
targets is a vector of Gtk::TargetEntry elements.
start_button_mask is an ORed combination of values, which specify which modifier key or mouse button must be pressed to start the drag.
actions is an ORed combination of values, which specified which Drag and Drop operations will be possible from this source - for instance, copy, move, or link. The user can choose between the actions by using modifier keys, such as Shift to change from copy to move, and this will be shown by a different cursor.
void drag_dest_set(const std::vectorlt;Gtk::TargetEntrygt;amp; targets, Gtk::DestDefaults flags, Gdk::DragAction actions);
flags is an ORed combination of values which indicates how the widget will respond visually to Drag and Drop items.
actions indicates the Drag and Drop actions which this destination can receive - see the description above.
When a drop destination has accepted a dragged item, certain signals will be emitted, depending on what action has been selected. For instance, the user might have held down the Shift key to specify a move rather than a copy. Remember that the user can only select the actions which you have specified in your calls to drag_dest_set() and drag_source_set().
Copy
drag_begin: Provides DragContext.
drag_data_get: Provides info about the dragged data format, and a Gtk::SelectionData structure, in which you should put the requested data.
drag_end: Provides DragContext.
The source widget will emit these signals, in this order:
drag_motion: Provides DragContext and coordinates. You can call the drag_status() method of the DragContext to indicate which action will be accepted.
drag_drop: Provides DragContext and coordinates. You can call drag_get_data(), which triggers the drag_data_get signal in the source widget, and then the drag_data_received signal in the destination widget.
drag_data_received: Provides info about the dragged data format, and a Gtk::SelectionData structure which contains the dropped data. You should call the drag_finish() method of the DragContext to indicate whether the operation was successful.
The destination widget will emit these signals, in this order:
Move
drag_data_delete: Gives the source the opportunity to delete the original data if that's appropriate.
During a move, the source widget will also emit this signal:
DragContext
The drag and drop signals provide a DragContext, which contains some information about the drag and drop operation and can be used to influence the process. For instance, you can discover the source widget, or change the drag and drop icon, by using the set_icon() methods. More importantly, you should call the drag_finish() method from your drag_data_received signal handler to indicate whether the drop was successful.
Here is a very simple example, demonstrating a drag and drop Copy operation:
There is a more complex example in examples/others/dnd.
The Clipboard
Simple text copy-paste functionality is provided for free by widgets such as Gtk::Entry and Gtk::TextView, but you might need special code to deal with your own data formats. For instance, a drawing program would need special code to allow copy and paste within a view, or between documents.
You can usually pretend that Gtk::Clipboard is a singleton. You can get the default clipboard instance with Gtk::Clipboard::get(). This is probably the only clipboard you will ever need.
Your application doesn't need to wait for clipboard operations, particularly between the time when the user chooses Copy and then later chooses Paste. Most Gtk::Clipboard methods take sigc::slots which specify callback methods. When Gtk::Clipboard is ready, it will call these methods, either providing the requested data, or asking for data.
Targets
Different applications contain different types of data, and they might make that data available in a variety of formats. gtkmm calls these data types targets.
For instance, gedit can supply and receive the "UTF8_STRING" target, so you can paste data into gedit from any application that supplies that target. Or two different image editing applications might supply and receive a variety of image formats as targets. As long as one application can receive one of the targets that the other supplies then you will be able to copy data from one to the other.
A target can be in a variety of binary formats. This chapter, and the examples, assume that the data is 8-bit text. This would allow us to use an XML format for the clipboard data. However this would probably not be appropriate for binary data such as images. Gtk::Clipboard provides overloads that allow you to specify the format in more detail if necessary.
The Drag and Drop API uses the same mechanism. You should probably use the same data targets and formats for both Clipboard and Drag and Drop operations.
When the user asks to copy some data, you should tell the Clipboard what targets are available, and provide the callback methods that it can use to get the data. At this point you should store a copy of the data, to be provided when the clipboard calls your callback method in response to a paste.
Glib::RefPtrlt;Gtk::Clipboardgt; refClipboard = Gtk::Clipboard::get(); //Targets: std::vectorlt;Gtk::TargetEntrygt; targets; targets.push_back( Gtk::TargetEntry("example_custom_target") ); targets.push_back( Gtk::TargetEntry("UTF8_STRING") ); refClipboard-gt;set( targets, sigc::mem_fun(*this, &ExampleWindow::on_clipboard_get), sigc::mem_fun(*this, &ExampleWindow::on_clipboard_clear) );
Your callback will then provide the stored data when the user chooses to paste the data. For instance:
void ExampleWindow::on_clipboard_get( Gtk::SelectionDataamp; selection_data, guint /* info */) { const std::string target = selection_data.get_target(); if(target == "example_custom_target") selection_data.set("example_custom_target", m_ClipboardStore); }
The ideal example below can supply more than one clipboard target.
The clear callback allows you to free the memory used by your stored data when the clipboard replaces its data with something else.
Paste
When the user asks to paste data from the Clipboard, you should request a specific format and provide a callback method which will be called with the actual data. For instance:
refClipboard-gt;request_contents("example_custom_target", sigc::mem_fun(*this, &ExampleWindow::on_clipboard_received) );
Here is an example callback method:
void ExampleWindow::on_clipboard_received( const Gtk::SelectionDataamp; selection_data) { Glib::ustring clipboard_data = selection_data.get_data_as_string(); //Do something with the pasted data. }
Discovering the available targets
To find out what targets are currently available on the Clipboard for pasting, call the request_targets() method, specifying a method to be called with the information. For instance:
refClipboard-gt;request_targets( sigc::mem_fun(*this, &ExampleWindow::on_clipboard_received_targets) );
In your callback, compare the vector of available targets with those that your application supports for pasting. You could enable or disable a Paste menu item, depending on whether pasting is currently possible. For instance:
void ExampleWindow::on_clipboard_received_targets( const std::vectorlt;Glib::ustringgt;amp; targets) { const bool bPasteIsPossible = std::find(targets.begin(), targets.end(), example_target_custom) != targets.end(); // Enable/Disable the Paste button appropriately: m_Button_Paste.set_sensitive(bPasteIsPossible); }
Simple
This example allows copy and pasting of application-specific data, using the standard text target. Although this is simple, it's not ideal because it does not identify the Clipboard data as being of a particular type.
Clipboard - Simple
Ideal
Defines a custom clipboard target, though the format of that target is still text.
It supports pasting of 2 targets - both the custom one and a text one that creates an arbitrary text representation of the custom data.
It uses request_targets() and the owner_change signal and disables the Paste button if it can't use anything on the clipboard.
This is like the simple example, but it
Clipboard - Ideal
Printing
At the application development level, gtkmm's printing API provides dialogs that are consistent across applications and allows use of Cairo's common drawing API, with Pango-driven text rendering. In the implementation of this common API, platform-specific backends and printer-specific drivers are used.
PrintOperation
The primary object is Gtk::PrintOperation, allocated for each print operation. To handle page drawing connect to its signals, or inherit from it and override the default virtual signal handlers. PrintOperation automatically handles all the settings affecting the print loop.
begin_print: You must handle this signal, because this is where you create and set up a Pango::Layout using the provided Gtk::PrintContext, and break up your printing output into pages.
paginate: Pagination is potentially slow so if you need to monitor it you can call the PrintOperation::set_show_progress() method and handle this signal.
request_page_setup: Provides a PrintContext, page number and Gtk::PageSetup. Handle this signal if you need to modify page setup on a per-page basis.
draw_page: You must handle this signal, which provides a PrintContext and a page number. The PrintContext should be used to create a Cairo::Context into which the provided page should be drawn. To render text, iterate over the Pango::Layout you created in the begin_print handler.
For each page that needs to be rendered, the following signals are emitted:
end_print: A handler for it is a safe place to free any resources related to a PrintOperation. If you have your custom class that inherits from PrintOperation, it is naturally simpler to do it in the destructor.
done: This signal is emitted when printing is finished, meaning when the print data is spooled. Note that the provided Gtk::PrintOperationResult may indicate that an error occurred. In any case you probably want to notify the user about the final status.
status_changed: Emitted whenever a print job's status changes, until it is finished. Call the PrintOperation::set_track_print_status() method to monitor the job status after spooling. To see the status, use get_status() or get_status_string().
The PrintOperation::run() method starts the print loop, during which various signals are emitted:
Page setup
The PrintOperation class has a method called set_default_page_setup() which selects the default paper size, orientation and margins. To show a page setup dialog from your application, use the Gtk::run_page_setup_dialog() method, which returns a Gtk::PageSetup object with the chosen settings. Use this object to update a PrintOperation and to access the selected Gtk::PaperSize, Gtk::PageOrientation and printer-specific margins.
You should save the chosen Gtk::PageSetup so you can use it again if the page setup dialog is shown again.
//Within a class that inherits from Gtk::Window and keeps m_refPageSetup and m_refSettings as members... Glib::RefPtrlt;Gtk::PageSetupgt; new_page_setup = Gtk::run_page_setup_dialog(*this, m_refPageSetup, m_refSettings); m_refPageSetup = new_page_setup;
For instance,
The Cairo coordinate system, in the draw_page handler, is automatically rotated to the current page orientation. It is normally within the printer margins, but you can change that via the PrintOperation::set_use_full_page() method. The default measurement unit is device pixels. To select other units, use the PrintOperation::set_unit() method.
Rendering text
Text rendering is done using Pango. The Pango::Layout object for printing should be created by calling the PrintContext::create_pango_layout() method. The PrintContext object also provides the page metrics, via get_width() and get_height(). The number of pages can be set with PrintOperation::set_n_pages(). To actually render the Pango text in on_draw_page, get a Cairo::Context with PrintContext::get_cairo_context() and show the Pango::LayoutLines that appear within the requested page number.
See an example of exactly how this can be done.
Asynchronous operations
By default, PrintOperation::run() returns when a print operation is completed. If you need to run a non-blocking print operation, call PrintOperation::set_allow_async(). Note that set_allow_async() is not supported on all platforms, however the done signal will still be emitted.
run() may return PRINT_OPERATION_RESULT_IN_PROGRESS. To track status and handle the result or error you need to implement signal handlers for the done and status_changed signals:
// in class ExampleWindow's method... Glib::RefPtrlt;PrintOperationgt; op = PrintOperation::create(); // ...set up op... op-gt;signal_done().connect(sigc::bind(sigc::mem_fun(*this, &ExampleWindow::on_printoperation_done), op)); // run the op
void ExampleWindow::on_printoperation_done(Gtk::PrintOperationResult result, const Glib::RefPtrlt;PrintOperationgt;amp; op) { if (result == Gtk::PRINT_OPERATION_RESULT_ERROR) //notify user else if (result == Gtk::PRINT_OPERATION_RESULT_APPLY) //Update PrintSettings with the ones used in this PrintOperation if (! op-gt;is_finished()) op-gt;signal_status_changed().connect(sigc::bind(sigc::mem_fun(*this, &ExampleWindow::on_printoperation_status_changed), op)); }
Second, check for an error and connect to the status_changed signal. For instance:
void ExampleWindow::on_printoperation_status_changed(const Glib::RefPtrlt;PrintFormOperationgt;amp; op) { if (op-gt;is_finished()) //the print job is finished else //get the status with get_status() or get_status_string() //update UI }
Finally, check the status. For instance,
Export to PDF
Glib::RefPtrlt;Gtk::PrintOperationgt; op = Gtk::PrintOperation::create(); // ...set up op... op-gt;set_export_filename("test.pdf"); Gtk::PrintOperationResult res = op-gt;run(Gtk::PRINT_OPERATION_ACTION_EXPORT);
The 'Print to file' option is available in the print dialog, without the need for extra implementation. However, it is sometimes useful to generate a pdf file directly from code. For instance,
Extending the print dialog
Set the title of the tab via PrintOperation::set_custom_tab_label(), create a new widget and return it from the create_custom_widget signal handler. You'll probably want this to be a container widget, packed with some others.
Get the data from the widgets in the custom_widget_apply signal handler.
You may add a custom tab to the print dialog:
Gtk::Widget* CustomPrintOperation::on_create_custom_widget() { set_custom_tab_label("My custom tab"); Gtk::Box* hbox = new Gtk::Box(Gtk::ORIENTATION_HORIZONTAL, 8); hbox-gt;set_border_width(6); Gtk::Label* label = Gtk::manage(new Gtk::Label("Enter some text: ")); hbox-gt;pack_start(*label, false, false); label-gt;show(); hbox-gt;pack_start(m_Entry, false, false); m_Entry.show(); return hbox; } void CustomPrintOperation::on_custom_widget_apply(Gtk::Widget* /* widget */) { Glib::ustring user_input = m_Entry.get_text(); //... }
Although the custom_widget_apply signal provides the widget you previously created, to simplify things you can keep the widgets you expect to contain some user input as class members. For example, let's say you have a Gtk::Entry called m_Entry as a member of your CustomPrintOperation class:
The example in examples/book/printing/advanced demonstrates this.
Preview
// in a class that inherits from Gtk::Window... Glib::RefPtrlt;PrintOperationgt; op = PrintOperation::create(); // ...set up op... op-gt;run(Gtk::PRINT_OPERATION_ACTION_PREVIEW, *this);
The native GTK+ print dialog has a preview button, but you may also start a preview directly from an application:
On Unix, the default preview handler uses an external viewer program. On Windows, the native preview dialog will be shown. If necessary you may override this behaviour and provide a custom preview dialog. See the example located in /examples/book/printing/advanced.
The following example demonstrates how to print some input from a user interface. It shows how to implement on_begin_print and on_draw_page, as well as how to track print status and update the print settings.
Printing - Simple
Recently Used Documents
gtkmm provides an easy way to manage recently used documents. The classes involved in implementing this functionality are RecentManager, RecentChooserDialog, RecentChooserMenu, RecentChooserWidget, RecentAction, and RecentFilter.
Each item in the list of recently used files is identified by its URI, and can have associated metadata. The metadata can be used to specify how the file should be displayed, a description of the file, its mime type, which application registered it, whether it's private to the registering application, and several other things.
RecentManager
RecentManager acts as a database of recently used files. You use this class to register new files, remove files from the list, or look up recently used files. There is one list of recently used files per user.
You can create a new RecentManager, but you'll most likely just want to use the default one. You can get a reference to the default RecentManager with get_default().
RecentManager is the model of a model-view pattern, where the view is a class that implements the RecentChooser interface.
Adding Items to the List of Recent Files
To add a new file to the list of recent documents, in the simplest case, you only need to provide the URI. For example:
Glib::RefPtrlt;Gtk::RecentManagergt; recent_manager = Gtk::RecentManager::get_default(); recent_manager-gt;add_item(uri);
If you want to register a file with metadata, you can pass a RecentManager::Data parameter to add_item(). The metadata that can be set on a particular file item is as follows:
app_exec: The command line to be used to launch this resource. This string may contain the "f" and "u" escape characters which will be expanded to the resource file path and URI respectively
app_name: The name of the application that registered the resource
description: A short description of the resource as a UTF-8 encoded string
display_name: The name of the resource to be used for display as a UTF-8 encoded string
groups: A list of groups associated with this item. Groups are essentially arbitrary strings associated with a particular resource. They can be thought of as 'categories' (such as "email", "graphics", etc) or tags for the resource.
is_private: Whether this resource should be visible only to applications that have registered it or not
mime_type: The MIME type of the resource
In addition to adding items to the list, you can also look up items from the list and modify or remove items.
Looking up Items in the List of Recent Files
To look up recently used files, RecentManager provides several functions. To look up a specific item by its URI, you can use the lookup_item() function, which will return a RecentInfo class. If the specified URI did not exist in the list of recent files, lookup_item() throws a RecentManagerError exception. For example:
Glib::RefPtrlt;Gtk::RecentInfogt; info; try { info = recent_manager-gt;lookup_item(uri); } catch(const Gtk::RecentManagerErroramp; ex) { std::cerr &lt;lt; "RecentManagerError: " &lt;lt; ex.what() &lt;lt; std::endl; } if (info) { // item was found }
A RecentInfo object is essentially an object containing all of the metadata about a single recently-used file. You can use this object to look up any of the properties listed above.
If you don't want to look for a specific URI, but instead want to get a list of all recently used items, RecentManager provides the get_items() function. The return value of this function is a std::vector of all recently used files. The following code demonstrates how you might get a list of recently used files:
std::vectorlt; Glib::RefPtrlt;Gtk::RecentInfogt; &gt; info_list = recent_manager-gt;get_items();
The maximum age of items in the recently used files list can be set with Gtk::Settings::property_gtk_recent_files_max_age(). Default value: 30 days.
Modifying the List of Recent Files
There may be times when you need to modify the list of recent files. For instance, if a file is moved or renamed, you may need to update the file's location in the recent files list so that it doesn't point to an incorrect location. You can update an item's location by using move_item().
In addition to changing a file's URI, you can also remove items from the list, either one at a time or by clearing them all at once. The former is accomplished with remove_item(), the latter with purge_items().
The functions move_item(), remove_item() and purge_items() have no effect on the actual files that are referred to by the URIs, they only modify the list of recent files.
RecentChooser
RecentChooser is an interface that can be implemented by widgets displaying the list of recently used files. gtkmm provides four built-in implementations for choosing recent files: RecentChooserWidget, RecentChooserDialog, RecentChooserMenu, and RecentAction.
RecentChooserWidget is a simple widget for displaying a list of recently used files. RecentChooserWidget is the basic building block for RecentChooserDialog, but you can embed it into your user interface if you want to.
RecentChooserMenu and RecentAction allow you to list recently used files as a menu.
Simple RecentChooserDialog example
Shown below is a simple example of how to use the RecentChooserDialog and the RecentAction classes in a program. This simple program has a menubar with a Recent Files Dialog menu item. When you select this menu item, a dialog pops up showing the list of recently used files.
If this is the first time you're using a program that uses the Recent Files framework, the dialog may be empty at first. Otherwise it should show the list of recently used documents registered by other applications.
