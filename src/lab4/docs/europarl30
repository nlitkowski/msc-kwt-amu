The vote will take place shortly, when we have concluded the debates.
The next item is the joint debate on the following motions for resolutions on human rights in Zimbabwe:
?5-0112/2003 by Mr Van Orden, Mr Corrie, Mr Deva, Mrs Foster, Mr Parish, Mr Tannock, Mrs Banotti, Mr Gahler, Mrs Korhola, Mr Lehne, Mrs Maij-Weggen, Mr Posselt and Mr Sacrédeus, on behalf of the PPE-DE Group, on Zimbabwe;
?5-0138/2003 by Mr Sylla, Mr Cossutta and Mr Miranda, on behalf of the GUE/NGL Group, on Zimbabwe;
?5-0142/2003 by Mrs Kinnock and Mr van den Berg, on behalf of the PSE Group, on the human rights situation in Zimbabwe;
?5-0145/2003 by Mrs Maes, Mr Rod, Mrs Lucas, Mr Lannoye, Mrs Schörling and Mrs Isler Béguin, on behalf of the Verts/ALE Group, on the human rights situation in Zimbabwe;
?5-0148/2003 by Mr van den Bos, on behalf of the ELDR Group, on the human rights situation in Zimbabwe;
?5-0151/2003 by Mr Belder, on behalf of the EDD Group, on the situation in Zimbabwe.
Mr President, I speak on behalf of my friend, Mr Van Orden, and the British Conservatives.
I also speak on behalf of the oppressed people of Zimbabwe who are crying out for justice.
Yet again we find ourselves having to discuss Zimbabwe.
Why, one might ask, are we doing so now?
The situation in Zimbabwe has gone from bad to worse, with enforced starvation and continuing human rights abuses and political oppression.
However, the most urgent issue at this moment relates to action to be taken by the EU itself.
It is unbelievable that there was a need for a lengthy Council discussion on the renewal of sanctions, which must take effect by 18 February.
Who would believe that their renewal would depend upon the Council's agreement to Mugabe visiting Paris!
Mugabe is the prime instigator of the oppression and misery of the Zimbabwean people and the supposed main target of EU action.
The problem with EU sanctions has been their lack of rigorous enforcement.
Now the plan is to allow Mugabe and his entourage to travel to Paris.
No wonder that the EU is not taken seriously.
After all, we should not be trying to find ways of helping Mugabe to evade our own sanctions.
Instead we should be trying to make sanctions more effective.
If anything, they need to be widened in their scope!
We should not be surprised if African leaders are half-hearted in their support for international measures when the EU is seen to lack resolve.
The EU's weakness is in marked contrast to the brave stand taken by the Zimbabwean cricketers, Henry Olonga and Andy Flower.
Prior to taking the field for Zimbabwe's opening match of the World Cricket Cup, they said: 'we cannot in good conscience take to the field and ignore the fact that millions of our compatriots are (...) oppressed. (...) we are making a silent plea to those responsible to stop the abuse of human rights in Zimbabwe.
In so doing, we pray that our small action may help restore sanity and dignity to our nation.'
They have put their lives and their livelihoods at risk by their courage.
They deserve all the support we can give to them and to the suffering people of Zimbabwe.
I call upon Members to support the resolution without amendment.
Mr President, ladies and gentlemen, I believe that this is an example of exactly what we must not do.
What Mr Deva has just said is the opposite of what must be done in this type of case.
I think, on the contrary, that we have made a serious mistake.
Recognising human rights abuses in Zimbabwe is one thing, but assisting the opposition and doing all we can to establish a democracy is quite another.
Lastly, the increase in sanctions proposed by Mr Deva is inappropriate in a country riddled with famine and pandemics, because these sanctions will not affect Mr Mugabe, but the people.
I still believe that our serious mistake was precisely cancelling the sitting of the equal ACP-European Union assembly.
When Belgium grants a visa to nationals of a country, when the delegates concerned are present, when others then have the right to take part in a meeting and when we unilaterally and dictatorially refuse them access to this building, we deprive ourselves of the possibility for Africans and Europeans to adopt together a resolution that condemns what is happening in Zimbabwe.
Instead of that, we have strengthened all those who support Mr Mugabe.
We have provided him with superb publicity.
What is not responsible is for you to say that France has no right to prohibit a man from taking part in a Franco-African summit in which he is involved, precisely because that would enable us to tell him what we think face to face.
If we want lasting peace in this region, if we do not want discrimination and violence against the poorest to become even more widespread in these countries, we need to involve the regional organisations, and in particular the African Union, in our action.
In order to do that, we need to sit down with them and hold discussions with them.
What you are doing here is forgetting the colonial past of a country in which there is certainly violence today against white farmers, but which is currently also paying the cost, it should be recognised, of that colonial past where, for many years, the vast majority of the black population did not have access to land, whereas today the country is the subject of agricultural reform.
We cannot, therefore, solve the problems by trying to attack Mr Mugabe, but by sitting down together around a table, and I completely disagree with you and your method.
Mr President, there has been a little confusion.
Mr Sylla was not one of the authors of the resolution.
He is opposing the resolution.
I do not know how that mistake was made.
We in the PSE Group are supporting the resolution and will be opposing the amendment from Mr Posselt, because it waters down the criticism of Mr Mugabe.
Zimbabwe is no longer a democratic country.
Parliamentary, presidential and all local elections recently have been characterised by intimidation, repression, voting fraud and state-sponsored political violence.
We have a situation where over 7 million Zimbabweans - over half the population - are on the brink of starvation.
If you want government-controlled food, you are required to have a ZANU-PF membership card.
Unemployment is running at 70%, inflation is at over 100% and 50% of the land is no longer being farmed.
We also have the usual African problem of AIDS.
Parliament has consistently called for a widening and rigorous enforcement of sanctions, as well as other measures, to make international action against the Mugabe regime more effective.
This resolution condemns the lack of coherence in EU policy and calls on the Council and governments of the Member States not to seek exemptions from the EU's own sanction regime, which can be misread.
Sanctions against the Mugabe regime should continue without interruption and without exemption.
The charges against the opposition, against Morgan Tsvangirai, are spurious and unsubstantiated.
We want to extend the existing sanctions to make them stronger and more effective.
We want the Council and Commission to provide more information on the freezing of the bank accounts of those subject to sanctions.
We want wider sanctions against the Zimbabwean regime, including an international sports and culture boycott.
That would not increase the suffering of the population of Zimbabwe.
We praise the courage of the Zimbabwean cricketers - Andy Flower and Henry Olonga - for wearing those black armbands to symbolise the lack of democracy and human rights in Zimbabwe.
We support the belated stance of the England cricket team which has now refused to play in Zimbabwe.
It is time for further action to be taken and for the sanctions to be strengthened and reinforced.
Mr President, let me say, for the sake of precision, that I am not one of the authors of the joint resolution, I am an author of one resolution.
The resolution we submitted was intended to vigorously denounce the human rights situation in Zimbabwe and specifically to emphasise the risk to which an unfair trial, exposes Mr Tsvangirai, the man who, under normal circumstances, would have been president of his country if the elections had not been tampered with.
Nonetheless, I cannot use my remaining seconds to denounce the human rights situation even further, because our Parliament prefers to repeat everything again and to place the emphasis on tightening sanctions again, while all the Member States of the European Union, including Britain, keep on invoking Article 3, paragraph 3 of which states that exceptions should be made for international meetings that have to be accommodated.
In the way it applies this article and paragraph 3, the European Union has once again decided that Mugabe should come to Paris for the summit next week to speak on the human rights situation that is on the agenda there.
How in God's name can we get out of this ambiguous situation?
This matter is once again dividing us, even though together we want to condemn the sanctions.
We cannot, however, have a situation where we in the European Parliament systematically want more sanctions than any of the Member States to which we belong, if the whole Council, which is again, incidentally, absent, does not intervene and does not opt for an unambiguous solution.
My group resolutely argues in favour of maintaining the dialogue, and of promulgating sanctions that can be imposed, but not of invoking sanctions that rule out any dialogue.
We have just heard how things are in Venezuela, where there is no dialogue.
They are asking for a dialogue there; in this case a dialogue is actually necessary!
There are many paragraphs in our resolution that we would like to see approved.
It follows, though, that you should not expect heroism from countries whose economies are so strongly intermeshed with that of Zimbabwe, which is bigger than that of our own European Member States, who do not even have the courage to get together to ask a UN representative to investigate the human rights situation in situ in Zimbabwe.
We will therefore be voting against several of these paragraphs, and we will also not be able to approve the resolution in its entirety, but we will be continuing to fight the same fight in favour of human rights and against this pernicious regime.
Mr President, there will never be a European foreign policy as long as Member States continue to follow their own agenda.
Not even a Convention will help bring one about.
The European Union is not even capable of pursuing a consistent, vigorous, clear policy with regard to Zimbabwe.
The scandalous human rights situation, political repression, the economic decline and starvation ultimately count for less than French interests in Africa.
We must not forget that our severe sanctions are the only alternative to punishments that affect the population.
The people of Zimbabwe are already suffering far too much.
Yet France is riding roughshod over the agreements by inviting Mugabe, and in doing so is putting the credibility of the European Union on the line.
It now looks as if the Council is intending to relax the sanctions by introducing exception provisions.
The opposite is, however, urgently needed.
Rather than being interrupted, the sanctions against the regime must be extended and intensified, with absolutely no visas at all for Mr Mugabe and his henchmen, and with cricket teams who want to play in Zimbabwe being declared all out before they even start.
Fortunately our Parliament is consistent, or at least most of the parties are, and we must keep it that way.
If Europe is not consistent, how can we expect African countries to be?
It is most unfortunate that South Africa and Nigeria seem to be standing up for Mugabe and want the Commonwealth's sanctions to be lifted.
Free, honest elections are needed soon.
Show trials against opposition leaders must be stopped immediately, and the United Nations must appoint a special human rights rapporteur.
Only unconditional actions against the regime can release the people from their suffering.
The starving, terrorised victims of the former resistance hero must be able to count on our support.
It is time Member States put their own commercial agendas aside and pulled out all the stops for human rights on a European level.
State terrorism combined with corruption and drought are converging to create a national catastrophe.
The situation in Zimbabwe once again calls for an urgent debate.
Some 7.2 million Zimbabweans, more than half the population, are living on the brink of starvation.
The shocking aspect in all of this is the fact that the government is abusing the distribution of food as a weapon in its fight against its political opponents.
Fear currently prevails in the country.
As the economic crisis worsens, government repression is plumbing new depths.
Increasing numbers of cases of torture are being reported, including such things as the administration of electric shocks, poisoning and rape.
Being a supporter of the MDC is in itself life-threatening.
This is true of its leaders even more than of its sympathisers.
Morgan Tsvangirai, Welshman Ncube and Renson Gasela were recently accused of high treason for spurious reasons.
Against this sinister background, it is absolutely shocking that France has now invited this dictator to the Franco-African Summit conference in Paris.
The excuse that the summit starts on 19 February while the sanctions were originally in force until 18 February sounds lame.
On the basis of paragraph 12, therefore, I vigorously condemn this unilateral element in French policy.
It is not, at the moment, the only one.
Finally I would like to endorse the agreement that has at last been reached within the European Union on the extension of the sanctions by 12 months, but not without putting on record my protest against the French approach.
I call on the Council and Commission to unanimously implement the renewed sanctions in full without tolerating any opt outs.
Mr President, I might inform you that purely coincidentally the German Parliament is also debating Zimbabwe at this very hour today and I believe that, from a political point of view, it is important for us not only to discuss this issue here in the European Parliament - or perhaps also, for historical reasons, in London - but also in other parliaments in Europe, so as to raise public awareness of the situation.
Regrettably, it is necessary to extend the sanctions imposed on the Mugabe regime.
The use of democratic means has, unfortunately, not enabled the people there to rid themselves of a potentate who bears more responsibility than anyone else for the decline of the country in economic, social and political terms and also in respect of human rights.
I have to take issue with what Mr Sylla said.
The sanctions that we introduced do not actually affect the general public.
The only people that are hit by them are those who in any case have the money to travel.
But when, for example, the 'propaganda minister', Mr Moyo, went to South Africa on a shopping spree over Christmas, it caused a public outcry in that democratic society too that someone so close to the regime should be behaving in this way.
I very much regret the fact that the discussions about extending the sanctions degenerated into political horse-trading, because France would only agree to the extension if Mr Mugabe was allowed to come to Paris for the Franco-African summit.
I do not like to criticise a government whose political views are so close to my own, but in this case I would urge the French Government to consider whether it is in the interests of Africans to engage in African politics in this way.
Unfortunately, in previous decades for many European countries one single criterion has been decisive in determining whether they have got involved in African politics: whether it served their own economic interests.
The result is clear to see in countries such as Côte d'Ivoire and Congo.
On the contrary, it is important for Europe to present a united front.
I therefore welcome the fact that Portugal would rather postpone the EU-Africa summit than allow President Mugabe to travel here.
Moreover, at the 59th session of the UN Commission on Human Rights, the Europeans must carry out early consultations so as to ensure that any draft resolution on Zimbabwe does not fail to go through, as happened last year, but that it is removed from the agenda beforehand.
Mr President, Commissioner, ladies and gentlemen, this is not the first time I have spoken about the enduring and unhappy saga of Zimbabwe and its dictator Robert Mugabe.
Our position has not changed: we strongly condemn Mr Mugabe's regime, the extremely violent, ongoing and shameful breaches of human rights, the way in which he is destroying his own country and condemning his people to poverty.
This is intolerable and our condemnation is unswerving.
I would simply say, however, that we must now act intelligently.
A policy of sanctions has been in place for a year now.
It is too early to tell whether the outcome of this policy will be positive or negative, but whatever the outcome is, we all know what has been going on.
We also know that these policies of sanctions always require a process to be in place.
The question in my mind is this: given the difficulties that have arisen and that we ourselves have felt, with the suspension of the Joint ACP-European Union Assembly, which have also been felt in the Commonwealth, and which are also currently dividing Europe, would it not be better to act somewhat more intelligently?
I wish to propose an alternative plan, even though I have seen that a majority in this House has already been achieved!
Why do we not adopt a three-month moratorium, at the end of which, if Robert Mugabe's regime has not adopted serious measures to respect human rights, we would then decree a truly unanimous system of sanctions?
In the meantime, we could explore opportunities for EU-Africa dialogue, once again pulling the carpet out from under the feet of Mr Mugabe, rather than giving his propaganda sustenance ...
Furthermore, we should launch a massive movement of positive solidarity towards Morgan Tsvangirai, who is being unjustly and seriously persecuted in his country.
We in this House have the ability to do this and to mobilise European public opinion.
Mr Tsvangirai has, as you know, already been nominated twice for the Sakharov Prize, which Parliament awards each year.
Launching a massive movement of support for Morgan Tsvangirai to receive the Sakharov Prize at the end of this year would be much more effective than any policy of sanctions.
The Commission closely follows the human rights situation in Zimbabwe.
It is aware that the conditions remain volatile and is deeply concerned regarding the increased incidence of politically motivated violence observed recently.
In view of the gravity of the situation, the Commission proposed to the Council the extension of the measures against Zimbabwe under Article 96 of the Cotonou Agreement, which were adopted in February last year.
These measures redirect funds to social sectors and to areas which benefit directly the population.
This is for a renewable period of one year.
The Commission agrees with the Presidency's proposal for a new common position amending and extending sanctions, such as the visa ban and freeze of assets against Zimbabwe, for the same additional period of one year.
The Commission is informed that approximately EUR 770 000 have been frozen in accounts in Member States or their dependent territories in accordance with the Council's position.
Community assistance continues to contribute to projects which directly support the population in the field of democratisation, respect for human rights and the rule of law; the European Initiative for Democracy and Human Rights includes Zimbabwe as a focal country.
The Commission is particularly concerned about the food crisis in Zimbabwe and has been very actively involved in responding to the situation.
Between April and December 2002, we committed about EUR 80 million in emergency aid and humanitarian aid.
The Commission believes that all avenues for dialogue should be explored to improve the situation in Zimbabwe.
We will support any peer pressure that the international community, and the heads of state in Africa in particular, may put on the Zimbabwean Government in the area of respect for the essential elements of the Cotonou Agreement: human rights, democracy and the rule of law.
The debate is closed.
We shall now proceed to the vote.
The next item is the vote on the crisis in the steel sector
Mr President, I welcome the vote on the resolution and would like to emphasise two points.
First of all, I would like to respond to the proposals put forward by the Commissioner earlier on, by drawing his attention to the fact that Article 5 of the Treaty on the ECSC stipulates that the role of the Community is to shed light and facilitate the action of interested parties by assembling information, organising discussions and defining general objectives.
It seems to me that, with regard to Walloon regional power, the Commission should consider such action.
Secondly, I feel that, following the various reports that the Commission could present to us, it could be useful for Parliament to draw up an own-initiative report on the development of metalworking in the face of what will undoubtedly be at stake in the enlargement process.
.
(FR) On behalf of us all, Mrs Laguiller expressed opposition in her speech to the business closures and redundancies planned by Arcelor.
The only way to prevent finance groups from acting is to prohibit mass redundancies and guarantee workers' salaries by taking funds from profits.
The compromise text, which is limited to a few protectionist measures and additional regulations which employers will not observe in any case, does not propose any practical measures for the protection of workers.
It is therefore out of the question for us to sanction this text.
The only reason we did not vote against it is because it proposes a few additional prerogatives for trade unions and we did not wish to oppose these.
Redundancies, however, are not more justified simply because the trade unions have been consulted.
I declare the session of the European Parliament adjourned
We Swedish Social Democrats chose to vote against quite a number of the amendments in the report concerning monitoring of forests and environmental interactions in the Community (Forest Focus).
We are very hesitant about giving Community policy a larger role when it comes to forests.
We believe that the basic conditions within the EU vary so much that it is questionable what added value such a Community policy would bring.
On the basis of that approach, we chose to vote against those items concerned with incorporating the prevention of forest fires into the regulation.
The risk is that, in the interests of monitoring, this would divert a lot of resources from other areas.
What is more, fire prevention measures are already a part of the EU Regulation on Rural Development.
In the final vote, we chose, in spite of our hesitations, to vote in favour of the report.
We did so in view of the fact that Parliament had included important wordings concerning the principle of subsidiarity and the importance of the Member States' participating in the work on developing this regulation.
.
Whatever some in this Parliament may say or wish, the European Union has absolutely no legal competence on the subject of abortion.
Each person has the right to their own views, and I do not deny that different ethical frameworks exist in different Member States.
This is not the issue.
The issue is where and how the European Union has a legal right to act.
In this respect, the report proposes an outrageous abuse of power and competence by the Union.
To legislate that EU monies could be used for the purposes of providing any type of abortion service would be an attack on the sovereignty of Member States' rights to legislate on the matter.
As an Irish Member of Parliament, I cannot support such a monstrous violation of the constitutional right of the people I represent to decide policy with regard to abortion.
This is a right which is enshrined in the Maastricht Treaty and one which I will defend vigorously and unequivocally.
Equally, it is wrong to attempt to impose abortion on countries that do not want it, and it is an error to give the signal that this Parliament would only support countries that facilitate abortion, which is not correct.
Together with the Group of the European People's Party (Christian Democrats) and European Democrats, I had voted in favour of Amendment No 53, which was however rejected, with 181 votes in favour, 264 votes against and 11 abstentions.
The report would have gained a lot if Amendment No 53 had instead been approved.
The concept of 'reproductive and sexual health and rights' would then have been clearly defined, in terms of the protection of the unborn child, by means of the following wording: 'In the context of this Regulation, abortion, which can never be promoted as a family planning method, cannot be considered as a service which guarantees sexual and reproductive health'.
Regrettably, there now instead remains uncertainty as to whether, on the directions of the EU and with the help of the EU's financial resources, aid organisations, invoking 'reproductive and sexual health care and services', also carry out abortions as a part of the aid they provide, in spite of this being illegal in certain Member States of the EU such as Ireland and Portugal.
It also remains unclear as to whether this activity de facto takes place in developing countries, in spite of similar legal protection of the life of the foetus, that is to say in direct violation of the recipient country's legal view.
We Swedish Social Democrats chose to vote against the report on the prevention and reduction of risks associated with drug dependence, and this in view of the fact that Parliament voted through a number of amendments directly or indirectly entailing social acceptance of drug dependence.
Right from the beginning, our attitude towards the recommendation has been that it focuses to an unduly high degree upon harm reduction.
Our point of departure is that the most important measure for reducing the risks associated with drug dependence is that of preventing dependence itself.
The fact that, in its vote, Parliament chose to support the wordings from the committee on the importance of preventive activity and the drugs-free rehabilitation of drug abusers is encouraging, but is insufficient to merit approval on our part.
Movement of persons with a long-stay visa (debate)
The next item is the report by Carlos Coelho, on behalf of the Committee on Civil Liberties, Justice and Home Affairs, on the proposal for a regulation of the European Parliament and of the Council amending the Convention implementing the Schengen Agreement and Regulation (EC) No 562/2006 as regards movement of persons with a long-stay visa - C6-0076/2009 -.
Mr President, Commissioner, ladies and gentlemen, today we are speaking about absurd situations such as that of a student who obtains a visa in order to take a course in Belgium.
Not falling within the remit of Directive 2004/114/EC, he may not go to the Netherlands to gather information from a specialised library for the purposes of writing his thesis nor benefit from a weekend getting to know Barcelona because he will be arrested in the country which issued the visa.
