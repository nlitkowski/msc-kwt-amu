DLNA profile '%s' not supported
Invalid date format: %s
Invalid date: %s
Cannot create object of class '%s': Not supported
Error from container '%s' on trying to find the newly added child object '%s' in it: %s
'ContainerID' agument missing.
'ObjectID' argument missing.
Failed to create object under '%s': %s
Invalid search criteria given
Failed to search in '%s': %s
No subtitle available
No thumbnailer available: %s
Thumbnailing not supported
No thumbnail available
No D-Bus thumbnailer available
No transcoder available for target format '%s'
Could not create GstElement for URI %s
Failed to create pipeline
Failed to link %s to %s
Failed to link pad %s to %s
Error from pipeline %s: %s
Warning from pipeline %s: %s
Failed to seek to offsets %lld:%lld
Failed to seek
Failed to create GStreamer data source for %s: %s
Could not create a transcoder configuration. Your GStreamer installation might be missing a plug-in
Required element %s missing
Time-based seek not supported
Network Interfaces
Disable transcoding
Disallow upload
Disallow deletion
Comma-separated list of domain:level pairs. See rygel(1) for details
Plugin Path
Engine Path
Disable plugin
Set plugin titles
Set plugin options
Disable UPnP (streaming-only)
Use configuration file instead of user configuration
Shutdown remote Rygel reference
Shutting down remote Rygel instance
Failed to shut-down other rygel instance: %s
Rygel is running in streaming-only mode.
Rygel v%s starting…
Failed to create root device factory: %s
Failed to create RootDevice for %s. Reason: %s
Failed to load user configuration: %s
Failed to load user configuration from file '%s': %s
No value available for '%s'
Value of '%s' out of range
Failed to create preferences dialog: %s
Failed to save configuration data to file '%s': %s
Failed to start Rygel service: %s
Failed to stop Rygel service: %s
Not Found
%%% Object creation in %s not allowed
Enable sharing of media, such as photos, videos and music, with DLNA
Select the network interface that DLNA media will be shared on, or share media on all interfaces
Any
XML node '%s' not found.
LibRygelRenderer
LibRygelServer
Programming with gtkmm 3
Murray
Cumming
Bernhard
Rieder
Chapter on "Timeouts".
Jonathon
Jongsma
Chapter on "Drawing with Cairo".
Chapter on "Working with gtkmm's Source Code".
Chapter on "Recent Files".
Ole
Laursen
Parts of chapter on "Internationalization".
Marko
Anastasov
Chapter on "Printing".
Daniel
Elstner
Section "Build Structure" of chapter on "Wrapping C Libraries with gmmproc".
Chris
Vine
Chapter on "Multi-threaded programs".
David
King
Section on Gtk::Grid.
Pedro
Ferreira
Chapter on Keyboard Events.
Kjell
Ahlstedt
Parts of the update from gtkmm 2 to gtkmm 3.
This book explains key concepts of the gtkmm C++ API for creating user interfaces. It also introduces the main user interface elements ("widgets").
2002, 2003, 2004, 2005, 2006, 2007, 2008, 2009, 2010
Murray Cumming
Permission is granted to copy, distribute and/or modify this document under the terms of the GNU Free Documentation License, Version 1.2 or any later version published by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts. You may obtain a copy of the GNU Free Documentation License from the Free Software Foundation by visiting their Web site or by writing to: Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
Introduction
This book
This book explains key concepts of the gtkmm C++ API for creating user interfaces. It also introduces the main user interface elements ("widgets"). Although it mentions classes, constructors, and methods, it does not go into great detail. Therefore, for full API information you should follow the links into the reference documentation.
This book assumes a good understanding of C++, and how to create C++ programs.
We would very much like to hear of any problems you have learning gtkmm with this document, and would appreciate input regarding improvements. Please see the Contributing section for further information.
gtkmm
gtkmm is a C++ wrapper for GTK+, a library used to create graphical user interfaces. It is licensed using the LGPL license, so you can develop open software, free software, or even commercial non-free software using gtkmm without purchasing licenses.
gtkmm was originally named gtk-- because GTK+ already has a + in the name. However, as -- is not easily indexed by search engines the package generally went by the name gtkmm, and that's what we stuck with.
Why use gtkmm instead of GTK+?
gtkmm allows you to write code using normal C++ techniques such as encapsulation, derivation, and polymorphism. As a C++ programmer you probably already realise that this leads to clearer and better organized code.
gtkmm is more type-safe, so the compiler can detect errors that would only be detected at run time when using C. This use of specific types also makes the API clearer because you can see what types should be used just by looking at a method's declaration.
Inheritance can be used to derive new widgets. The derivation of new widgets in GTK+ C code is so complicated and error prone that almost no C coders do it. As a C++ developer you know that derivation is an essential Object Orientated technique.
Member instances can be used, simplifying memory management. All GTK+ C widgets are dealt with by use of pointers. As a C++ coder you know that pointers should be avoided where possible.
gtkmm involves less code compared to GTK+, which uses prefixed function names and lots of cast macros.
gtkmm compared to Qt
Trolltech's Qt is the closest competition to gtkmm, so it deserves discussion.
gtkmm developers tend to prefer gtkmm to Qt because gtkmm does things in a more C++ way. Qt originates from a time when C++ and the standard library were not standardised or well supported by compilers. It therefore duplicates a lot of stuff that is now in the standard library, such as containers and type information. Most significantly, Trolltech modified the C++ language to provide signals, so that Qt classes cannot be used easily with non-Qt classes. gtkmm was able to use standard C++ to provide signals without changing the C++ language. See the FAQ for more detailed differences.
gtkmm is a wrapper
gtkmm is not a native C++ toolkit, but a C++ wrapper of a C toolkit. This separation of interface and implementation has advantages. The gtkmm developers spend most of their time talking about how gtkmm can present the clearest API, without awkward compromises due to obscure technical details. We contribute a little to the underlying GTK+ code base, but so do the C coders, and the Perl coders and the Python coders, etc. Therefore GTK+ benefits from a broader user base than language-specific toolkits - there are more implementers, more developers, more testers, and more users.
Installation
Dependencies
Before attempting to install gtkmm 3.0, you might first need to install these other packages.
libsigc++ 2.0
GTK+ 3.0
glibmm
cairomm
pangomm
atkmm
These dependencies have their own dependencies, including the following applications and libraries:
pkg-config
glib
ATK
Pango
cairo
gdk-pixbuf
Unix and Linux
Prebuilt Packages
Recent versions of gtkmm are packaged by nearly every major Linux distribution these days. So, if you use Linux, you can probably get started with gtkmm by installing the package from the official repository for your distribution. Distributions that include gtkmm in their repositories include Debian, Ubuntu, Red Hat, Fedora, Mandriva, Suse, and many others.
The names of the gtkmm packages vary from distribution to distribution (e.g. libgtkmm-3.0-dev on Debian and Ubuntu or gtkmm30-devel on Red Hat Fedora), so check with your distribution's package management program for the correct package name and install it like you would any other package.
The package names will not change when new API/ABI-compatible versions of gtkmm are released. Otherwise they would not be API/ABI-compatible. So don't be surprised, for instance, to find gtkmm 3.8 supplied by Debian's libgtkmm-3.0-dev package.
Installing From Source
If your distribution does not provide a pre-built gtkmm package, or if you want to install a different version than the one provided by your distribution, you can also install gtkmm from source. The source code for gtkmm can be downloaded from .
After you've installed all of the dependencies, download the gtkmm source code, unpack it, and change to the newly created directory. gtkmm can be built and installed with the following sequence of commands:
# ./configure # make # make install
Remember that on a Unix or Linux operating system, you will probably need to be root to install software. The su or sudo command will allow you to enter the root password and have root status temporarily.
The configure script will check to make sure all of the required dependencies are already installed. If you are missing any dependencies, it will exit and display an error.
By default, gtkmm will be installed under the /usr/local directory. On some systems you may need to install to a different location. For instance, on Red Hat Linux systems you might use the --prefix option with configure, like so: # ./configure --prefix=/usr
You should be very careful when installing to standard system prefixes such as /usr. Linux distributions install software packages to /usr, so installing a source package to this prefix could corrupt or conflict with software installed using your distribution's package-management system. Ideally, you should use a separate prefix for all software you install from source.
If you want to help develop gtkmm or experiment with new features, you can also install gtkmm from git. Most users will never need to do this, but if you're interested in helping with gtkmm development, see the Working with gtkmm's Source Code appendix.
Microsoft Windows
GTK+ and gtkmm were designed to work well with Microsoft Windows, and the developers encourage its use on the win32 platform. However, Windows has no standard installation system for development libraries. Please see the Windows Installation page for Windows-specific installation instructions and notes.
Basics
This chapter will introduce some of the most important aspects of gtkmm coding. These will be demonstrated with simple working example code. However, this is just a taster, so you need to look at the other chapters for more substantial information.
Your existing knowledge of C++ will help you with gtkmm as it would with any library. Unless we state otherwise, you can expect gtkmm classes to behave like any other C++ class, and you can expect to use your existing C++ techniques with gtkmm classes.
Simple Example
To begin our introduction to gtkmm, we'll start with the simplest program possible. This program will create an empty 200 x 200 pixel window.
Source Code
We will now explain each line of the example
#include &lt;gtkmm.hgt;
All gtkmm programs must include certain gtkmm headers; gtkmm.h includes the entire gtkmm kit. This is usually not a good idea, because it includes a megabyte or so of headers, but for simple programs, it suffices.
Glib::RefPtrlt;Gtk::Applicationgt; app = Gtk::Application::create(argc, argv, "org.gtkmm.examples.base");
The next statement: creates a Gtk::Application object, stored in a RefPtr smartpointer. This is needed in all gtkmm applications. The create() method for this object initializes gtkmm, and checks the arguments passed to your application on the command line, looking for standard options such as --display. It takes these from the argument list, leaving anything it does not recognize for your application to parse or ignore. This ensures that all gtkmm applications accept the same set of standard arguments.
The next two lines of code create a window and set its default (initial) size:
Gtk::Window window; window.set_default_size(200, 200);
The last line shows the window and enters the gtkmm main processing loop, which will finish when the window is closed. Your main() function will then return with an appropriate success or error code.
return app-gt;run(window);
g++ simple.cc -o simple `pkg-config gtkmm-3.0 --cflags --libs`
After putting the source code in simple.cc you can compile the above program with gcc using: Note that you must surround the pkg-config invocation with backquotes. Backquotes cause the shell to execute the command inside them, and to use the command's output as part of the command line. Note also that simple.cc must come before the pkg-config invocation on the command line.
Headers and Linking
Although we have shown the compilation command for the simple example, you really should use the automake and autoconf tools, as described in "Autoconf, Automake, Libtool", by G. V. Vaughan et al. The examples used in this book are included in the gtkmm-documentation package, with appropriate build files, so we won't show the build commands in future. You'll just need to find the appropriate directory and type make.
To simplify compilation, we use pkg-config, which is present in all (properly installed) gtkmm installations. This program 'knows' what compiler switches are needed to compile programs that use gtkmm. The --cflags option causes pkg-config to output a list of include directories for the compiler to look in; the --libs option requests the list of libraries for the compiler to link with and the directories to find them in. Try running it from your shell-prompt to see the results on your system.
PKG_CHECK_MODULES([MYAPP], [gtkmm-3.0 &gt;= 3.8.0])
However, this is even simpler when using the PKG_CHECK_MODULES() macro in a standard configure.ac file with autoconf and automake. For instance: This checks for the presence of gtkmm and defines MYAPP_LIBS and MYAPP_CFLAGS for use in your Makefile.am files.
gtkmm-3.0 is the name of the current stable API. There was an older API called gtkmm-2-4 which installs in parallel when it is available. There were several versions of gtkmm-2.4, such as gtkmm 2.10 and there are several versions of the gtkmm-3.0 API. Note that the API name does not change for every version because that would be an incompatible API and ABI break. Theoretically, there might be a future gtkmm-4.0 API which would install in parallel with gtkmm-3.0 without affecting existing applications.
Note that if you mention extra modules in addition to gtkmm-3.0, they should be separated by spaces, not commas.
Openismus has more basic help with automake and autoconf.
Widgets
gtkmm applications consist of windows containing widgets, such as buttons and text boxes. In some other systems, widgets are called "controls". For each widget in your application's windows, there is a C++ object in your application's code. So you just need to call a method of the widget's class to affect the visible widget.
m_box.pack_start(m_Button1); m_box.pack_start(m_Button2);
m_frame.add(m_box);
Widgets are arranged inside container widgets such as frames and notebooks, in a hierarchy of widgets within widgets. Some of these container widgets, such as Gtk::Grid, are not visible - they exist only to arrange other widgets. Here is some example code that adds 2 Gtk::Button widgets to a Gtk::Box container widget: and here is how to add the Gtk::Box, containing those buttons, to a Gtk::Frame, which has a visible frame and title:
Most of the chapters in this book deal with specific widgets. See the Container Widgets section for more details about adding widgets to container widgets.
Although you can specify the layout and appearance of windows and widgets with C++ code, you will probably find it more convenient to design your user interfaces with Glade and load them at runtime with Gtk::Builder. See the Glade and Gtk::Builder chapter.
Although gtkmm widget instances have lifetimes and scopes just like those of other C++ classes, gtkmm has an optional time-saving feature that you will see in some of the examples. Gtk::manage() allows you to say that a child widget is owned by the container into which you place it. This allows you to new the widget, add it to the container and forget about deleting it. You can learn more about gtkmm memory management techniques in the Memory Management chapter.
Signals
gtkmm, like most GUI toolkits, is event-driven. When an event occurs, such as the press of a mouse button, the appropriate signal will be emitted by the Widget that was pressed. Each Widget has a different set of signals that it can emit. To make a button click result in an action, we set up a signal handler to catch the button's "clicked" signal.
m_button1.signal_clicked().connect( sigc::mem_fun(*this, &HelloWorld::on_button_clicked) );
gtkmm uses the libsigc++ library to implement signals. Here is an example line of code that connects a Gtk::Button's "clicked" signal with a signal handler called "on_button_clicked":
For more detailed information about signals, see the appendix.
For information about implementing your own signals rather than just connecting to the existing gtkmm signals, see the appendix.
Glib::ustring
You might be surprised to learn that gtkmm doesn't use std::string in its interfaces. Instead it uses Glib::ustring, which is so similar and unobtrusive that you could actually pretend that each Glib::ustring is a std::string and ignore the rest of this section. But read on if you want to use languages other than English in your application.
std::string uses 8 bit per character, but 8 bits aren't enough to encode languages such as Arabic, Chinese, and Japanese. Although the encodings for these languages have now been specified by the Unicode Consortium, the C and C++ languages do not yet provide any standardised Unicode support. GTK+ and GNOME chose to implement Unicode using UTF-8, and that's what is wrapped by Glib::ustring. It provides almost exactly the same interface as std::string, along with automatic conversions to and from std::string.
One of the benefits of UTF-8 is that you don't need to use it unless you want to, so you don't need to retrofit all of your code at once. std::string will still work for 7-bit ASCII strings. But when you try to localize your application for languages like Chinese, for instance, you will start to see strange errors, and possible crashes. Then all you need to do is start using Glib::ustring instead.
Note that UTF-8 isn't compatible with 8-bit encodings like ISO-8859-1. For instance, German umlauts are not in the ASCII range and need more than 1 byte in the UTF-8 encoding. If your code contains 8-bit string literals, you have to convert them to UTF-8 (e.g. the Bavarian greeting "Grüß Gott" would be "Gr\\xC3\\xBC\\xC3\\x9F Gott").
You should avoid C-style pointer arithmetic, and functions such as strlen(). In UTF-8, each character might need anywhere from 1 to 6 bytes, so it's not possible to assume that the next byte is another character. Glib::ustring worries about the details of this for you so you can use methods such as Glib::ustring::substr() while still thinking in terms of characters instead of bytes.
Unlike the Windows UCS-2 Unicode solution, this does not require any special compiler options to process string literals, and it does not result in Unicode executables and libraries which are incompatible with ASCII ones.
Reference
See the Internationalization section for information about providing the UTF-8 string literals.
Intermediate types
Some API related to gtkmm uses intermediate data containers, such as Glib::StringArrayHandle, instead of a specific Standard C++ container such as std::vector or std::list, though gtkmm itself now uses just std::vector since gtkmm 3.0.
