If it is just the colour or the aroma that is to be changed, then we do not need to go through this whole authorisation procedure again.
Studies must be made available under controlled conditions in order to avoid the duplication of animal experiments, but also, ultimately, to share the costs.
Only if research companies have the opportunity to recoup the high costs of testing active substances and getting products authorised through the marketing of the products will they also be prepared to invest in future.
All users must be fully aware of the effects of biocidal products, in particular those who use these products in a professional capacity on behalf of others.
If I try and get rid of lice on my head myself, that is at my own risk and I am doing it for myself.
However, if I go to a professional hairdresser for this purpose and pay him for it, then I expect him to have had the best possible training and the risk-free use of the products to ultimately be guaranteed.
Thus, his training must include information on the use of biocidal products.
The same applies to pest controllers, car body painters, painters and joiners.
As Parliament, we will not lose sight of the demand for a directive for the sustainable use of biocidal products - just as we will monitor the implementation of the regulation, which is now the responsibility of the Member States.
We are all aware that this sort of regulation is not set in stone.
There are sure to be more amendments and additions over time.
Even now, some issues remain unresolved.
For example, the demand for the delegated acts that give Parliament more rights of scrutiny in accordance with the new Treaty of Lisbon.
It is certainly the case that political decisions are often involved in the implementation, too.
It is important that Parliament be involved in the decision making when it comes to setting fees and with regard to the authorisation of active substances.
In these areas, we have not been granted the involvement that we are entitled to and therefore we will make it clear in a written statement that this does not set a precedent for future legislative acts.
Democracy requires the participation of elected representatives, in this case the participation of the European Parliament.
I would like once again to offer my sincere thanks to all of those who have contributed to this.
Member of the Commission. - Madam President, I would firstly like to thank Parliament, in particular the rapporteur, Christa Klaß, and also the shadows for their constructive engagement with the Council and the Commission.
This has resulted in a compromise text that provides an excellent basis for strengthening the internal market for biocidal products while at the same time ensuring a high level of protection for humans and the environment.
The Commission is also very happy with the pragmatic solutions that have been found for difficult issues, such as the scope of EU centralised procedure for products, the transitional arrangements for moving from existing directives to the new regulation and the rules for dealing with treated articles.
Throughout the negotiating process the Commission has insisted on the need to ensure that the European Chemicals Agency in Helsinki would have the necessary resources to carry out all the tasks it has been given under the regulation.
These have been expanded considerably during the course of the negotiations.
To this end, the Commission has produced a revised financial statement, and I would like to take this opportunity to remind Parliament of the need for the appropriate amounts to be included in the future budget allocations for the agency.
I would also like to thank you in advance.
While the Commission is supportive of the compromise text and does not wish to stand in the way of its adoption, there are two important issues which it would like to draw to the attention of Parliament.
Firstly, the Commission considers that, to be consistent with the Treaty, the regulation setting the fees to be paid to the chemical agency should be in the form of a delegated act rather than an implementing measure.
The Commission therefore informs Parliament that it reserves its right to pursue the legal remedies provided by the Treaty in order to seek clarification by the Court of Justice.
Secondly, in relation to the definition of nanomaterials, the Commission believes that it would have been better to include a simple cross-reference to the recent Commission recommendation on nanomaterials rather than to embed the definition in the text of the regulation itself.
Therefore the Commission would like to emphasise that the approach taken in relation to the biocides regulation should not be understood as constituting a precedent for future cases.
The Commission will make declarations on these two issues, and it will also make a declaration on setting the level of the fee for applications for mutual recognition payable to the European Chemicals Agency.
Having said this, I would like to make it clear that we regard the compromise text to be voted on during this plenary as an excellent basis, both for strengthening the internal market for biocidal products and for ensuring a high level of protection for humans and the environment.
This is the most important thing, is it not?
This regulation strikes the right balance.
It will ensure that we can develop safer products and use biocides efficiently to control the spread of harmful pests and germs, but it will also keep the most dangerous substances off the market, particularly those that cause cancer or fertility problems, and will also keep them out of the environment.
Thank you for your attention.
on behalf of the PPE Group. - (DE) Madam President, I would like to thank Ms Klaß for the work she has done here.
It was certainly a very difficult dossier, and we spent a long time grappling with it before we found a common line, particularly among the different groups.
Biocidal products must, by their very nature, involve active substances that entail a certain amount of danger, otherwise they would not have any effect.
Achieving the balance is the big challenge.
Our consumers cannot understand why many active substances should be more dangerous in some Member States than in others.
Therefore, it is also important from the point of view of the single market that we put this European authorisation system in place here.
It is very important that we make progress in this regard and make it clear to consumers that the single market should apply in this important area, too, and what is dangerous and prohibited in Denmark should not be authorised in other Member States like Italy or Portugal, either.
We have also clearly defined the substances for which we are prepared to accept exceptions, so that Member States that want to go a step further are actually able to do so.
We have surely therefore fulfilled our task of ensuring a high level of environmental and consumer protection while, on the other hand, also recognising the special characteristics of the Member States.
The importance of this matter is also demonstrated by the fact that, these days, we can probably say that there is hardly anything that we handle in our day-to-day lives that is not treated with these substances.
There have also been rumours recently that there are problems in various hotels in this wonderful city, where certain small animals are seemingly terrorising the place.
Here, too, we see that it is perhaps necessary to do something to combat these animals.
I just mention this in passing.
The fact is, we want to create this single market.
It is also the case that we are in favour of all products that are placed on our market only being permitted to be treated with substances that have a European authorisation.
Thus, no preference is being shown here to imported goods.
That is also important for our consumers to know.
We have also tightened up the requirements relating to animal experiments.
In this regard, results obtained by other producers must also be incorporated so that as few animals as possible are used.
Overall, this is a perfect compromise.
I think all of the groups have worked well together, and Ms Klaß has done a very good job here.
Madam President, every now and then, environmental organisations arrange media events where they present blood that they have tested - either from some of their staff or from well-known people, actors, singers or sometimes even from children.
Unfortunately, these tests always show that we are all walking around with chemical residues in our blood.
That is not all.
It is also possible to find chemical residues in the umbilical cord that connected a child to its mother.
Many of these residues come from very harmful chemicals, chemicals that are carcinogenic, endocrine disrupting, toxic to reproduction, bioaccumulating, resistant and toxic, in other words chemicals that are extremely dangerous.
This legislation is intended to help solve this problem.
It will ensure that we will get some of the most dangerous chemicals on the market replaced now.
What is more, these chemicals are in some products that we all surround ourselves with every day.
After all, biocides are actually a good thing.
There are biocides in detergents, that is quite sensible.
Biocides are used to impregnate textiles and furniture and so on - and that is also quite sensible.
However, it is clear that if we have biocides and chemicals in general so close to us in our everyday lives, it is important that they do not contain substances that are extremely dangerous.
It is therefore good that we have now adopted legislation that will ensure that we will phase out the most dangerous of these substances.
There are also a couple of other really good elements in this legislation.
We are strengthening the rules regarding the labelling requirements for the products.
We deal with nanomaterials in biocides in a different and more detailed way.
We have set out a plan for how, in the future - once the Commission has submitted a proposal for it - we are to have legislation regulating the use of biocides.
We have at last introduced the possibility of allowing a country - despite the fact that we have a common approval procedure - to be granted exemption and prohibit a pesticide where it may be hazardous on grounds of climatic or special national conditions when it might not be so in other countries.
I would therefore like to thank the Commission, the Council and my fellow Members, particularly the rapporteur, Ms Klaß.
on behalf of the ALDE Group. - (FR) Madam President, I would like to apologise on behalf of Corinne Lepage, our shadow rapporteur, who is unable to attend this evening because of the presidential campaign.
On her behalf, I would like to thank the rapporteur, Ms Klaß, who has had to leave for a few moments and who held her own in the Council during the second reading on biocides, and, of course, the shadow rapporteurs for their excellent collaboration.
Commissioner, Ms Lepage also wishes to thank the Commission for adopting such a constructive attitude and making every effort to promote the compromise that is on the table this evening.
I believe that we have succeeded in establishing a viable system that provides better protection for the environment and the health of users, and it was by no means a foregone conclusion.
In particular, we have ensured that the new central authorisation system does not result in a gradual blackout of the European Chemicals Agency in Helsinki.
The approach adopted - which is a three-stage approach - is the right one and it is also right that for September 2013 priority should be given to low-risk biocidal products and products used primarily for human hygiene and veterinary hygiene purposes.
As I said, the compromise thus improves the system on several fronts: clear criteria for excluding from the market products containing the most harmful substances, the establishment of provisional criteria for endocrine disrupters, better monitoring of nanomaterials, compulsory labelling of treated products and new rights for us as consumers, including the right to request information about these products from the manufacturers.
That was one of Parliament's priorities, aimed at preventing further scandals, such as the sofa scandal, which you might remember, whereby a large French company imported from China sofas containing unauthorised substances, which resulted in serious burns.
I do have one regret, however: the price Parliament has to pay for these improvements is giving up its right of veto concerning the amendment of the list of authorised active substances, and that is a great pity.
Nevertheless, on the whole, the Group of the Alliance of Socialists and Democrats in the European Parliament is obviously pleased that, with this regulation, with REACH and even with the Regulation on pesticides, Europe is today equipping itself with the world's most ambitious legislation on chemical products, nothing more and nothing less.
on behalf of the Verts/ALE Group. - (FR) Madam President, I, too, would like to congratulate the rapporteur, Ms Klaß, and the shadow rapporteurs, the Commission and the Polish Presidency on their constructive work.
From now on, consumers will be guaranteed better information.
Ms Ries spoke about the sofas imported from Asian countries that contained fungicides and we all know the consequences these had for consumers, including extensive skin problems and severe erythema.
Consumers should absolutely have been informed that items had been treated, but that was not the case until now.
For the Group of the Greens/European Free Alliance, it is essential to reinforce the substitution of highly dangerous active substances by strengthening the exclusion criteria but also by limiting the derogations and introducing national guarantees for the adjustment of the conditions of use of product authorisations.
We welcome, too, Commissioner, the specific and detailed provisions on nanomaterials, which you mentioned.
This is a very important matter because we will be informed, there will be labelling, and substances such as nano-silver will have to be specifically authorised and assessed separately, which is justification for the validity of the methods used for the supporting assessment.
The only damper on things - as has been mentioned - is that Parliament has lost its right of veto regarding the approval of active substances.
It would seem that that was the price we had to pay for these improvements.
on behalf of the ECR Group. - Madam President, this proposal on biocides is not perfect but it does present to us a perfect example of a European compromise.
The draft regulation emphasises the introduction of EU-wide authorisation procedures which should operate alongside national approval structures for biocidal products with similar conditions of use, which I welcome.
I also welcome the introduction of the concept of low-risk products with a simplified procedure.
This is all moving in the right direction but there are still some areas of concern.
I wonder particularly whether we have got the issue of confidentiality right.
Transparency is, of course, essential for consumer safety but innovation could be stifled by disproportionate disclosure demands.
It is important to be consistent, and I wonder whether the Commission is prepared to consider how this stacks up against the confidentiality and transparency issues in the REACH regulations.
Of course, the composition of products is only one issue.
It is also important to think about how they are used.
The proposal requires the Commission to come forward in three years' time with a report on how this regulation contributes to sustainable use, particularly for professional users, and I welcome this initiative from Parliament.
Balancing environmental and consumer protection and promoting the single market are always a regulatory challenge, but I think this proposal has moved us some way forward.
(DE) Madam President, ladies and gentlemen, the recommendation concerning the handling of biocidal products that is to be decided upon tomorrow is extremely important.
We encounter biocidal products not only in disinfectants or pesticides; they are also in furniture, lacquers and products that we handle every day.
They provide us with a very high level of protection in relation to hygiene.
It is therefore important for us to examine this proposal.
From the result, we can see that the highly toxic biocidal products will disappear from the market with this legislation.
We have established that the loopholes in the commercial chain will be closed by means of a clear labelling system.
I consider this to be extremely important.
Consumers will then know what they are buying.
However, it also demonstrates a high level of understanding in this area that, in addition to national defence, we are also able to allow exceptions in the area of animal disease control.
After all, in this area the threat to animals and people from animal epidemics or other diseases is very high.
It is therefore right and important for us to allow exceptions here.
This decision is justified by its proportionality.
However, I do not believe it is important for us in Europe to decide how these agents are used.
That is something that is subject to the principle of subsidiarity and should be decided by the authorities in the individual Member States.
Climate change, transport and many other external influences can deter the use of particular agents or prevent comprehensive implementation.
That is why we are leaving that up to the local authorities.
We will then surely succeed with regard to biocidal products and the protection of people against toxic substances.
(SV) Madam President, I would like to thank my fellow Members in Parliament who have contributed to our success in tightening up several aspects of the Commission's proposal.
I think it is extremely important that we have managed to expand the criteria for which substances are to be subject to the cut-off.
The clearer labelling that will now be introduced is also extremely important and this will hopefully deter consumers and thereby reduce the use of several questionable biocidal products.
What is the benefit of triclosan in children's underwear for example?
What informed consumer would buy that for his or her children?
However, these things currently appear to be a better choice because they are labelled as being 'antibacterial', and there are clearly some parents who believe that it is better to buy such things despite the fact that they pose a risk, even to their own children.
It worries me that this type of substance, which will possibly not fall within the cut-off criteria, is being used to an ever increasing extent, and therefore I look forward to the Commission presenting a new proposal for a directive on the sustainable use of biocidal products which will also tighten the rules for those substances that are not as dangerous as the very worst ones.
- (CS) Madam President, I would also like to thank the rapporteur, Christa Klaß, for a fine piece of work.
This report introduces new rules for placing biocidal products on the European market and brings a number of advantages, most of which have already been mentioned here.
These include mainly the right of consumers to information and a ban on the use of extremely hazardous materials, such as carcinogens, as well as the mandatory communication of data Europe-wide, which should prevent the duplication and multiplication of tests on animals, while simplifying data protection and harmonising the fees in individual Member States.
The regulation strengthens the position of the European Chemicals Agency, but at Member State level there are demands for retention of the power to withdraw permission to place a product on a Member State market on public health grounds.
The 'submission fees' paid to the European Chemicals Agency for submitting a request at national level have also become a legitimate target of criticism.
Despite this justified reservation, we are ready to support the regulation.
(IT) Madam President, ladies and gentlemen, I too would like to say how pleased I am that this text is finally at the approval stage.
I had the opportunity of working on an opinion for it as part of the Committee on the Internal Market and Consumer Protection (IMCO) in 2010 in conjunction with the Committee on the Environment, Public Health and Food Safety (ENVI).
I am especially pleased with the result and with the broad-based interinstitutional negotiations that have taken place over the last few months, as many issues that were adopted also in previous opinions are acknowledged here.
For this I would like to thank Ms Klaß for her excellent and I imagine tiring work.
We can confidently state that when this regulation comes into force it will ensure that internal procedures in the European single market are smoother, thanks to simpler authorisation procedures reinforced at EU level, as well as greater monitoring of human and environmental health, as many here have acknowledged.
The criteria for removing products that harm living organisms from the market have been reinforced, and at the same time there are exclusion clauses for certain products which have to be used but only when strictly necessary in safe and controlled conditions.
Therefore I am pleased with the result and would like to offer my thanks.
(IT) Madam President, Commissioner, ladies and gentlemen, it has not been easy to find a balanced compromise to combine the objective of greater protection for human health and the environment with a faster and more efficient authorisation system for the industrial sector.
However, I would have preferred a European authorisation from the European Chemicals Agency (ECHA) for new biocidal products immediately, and not in stages.
That said, the centralised authorisation system will definitely have positive repercussions on the internal market, because it will allow equal application of the requisites in all the Member States.
In order to streamline bureaucratic procedures for small and medium-sized enterprises and facilitate the entry of biocides onto the market, we needed to remove comparative assessment, still required for biocides that have been proven to be safe for use.
This places a hefty bureaucratic burden on companies.
Madam President, new legislation to replace the Biocidal Products Directive is absolutely necessary.
I therefore want to congratulate Christa Klaß and all my other colleagues who worked on this dossier, on being able to find common ground with the Council and to reach a compromise agreement.
I am particularly pleased with the provisions for clear labelling, labelling of nanomaterials and the recognition that excessive restrictions on certain products may result in a disproportionately negative impact on society.
However, I would urge the Commission and Member States to be wary of the cost of compliance with the new legislation.
We must make sure that the process of getting approval to use necessary products is not so great that the companies who need them cannot afford them.
Intimidating costs may mean that many SMEs in my constituency in Scotland, and those in other countries, feel excluded from the entire process.
We must ensure a level playing field and prevent dominant companies from excluding smaller competitors.
Just as excessive restrictions on certain products may result in a disproportionate negative impact on society, excessive costs of compliance will have a disproportionate impact on businesses, and particularly on SMEs, which make up a significant part of this sector.
(SK) Madam President, I strongly believe that simplifying the registration and authorisation of biocidal products should lead above all to a high level of health protection for people and the environment in which they live.
Special attention must be paid in this case to the protection of children and pregnant women.
The main threats to health are substances that have been identified as carcinogenic, mutagenic, toxic or toxic to reproduction and biocumulative, as well as substances with the properties of endocrine disruptors.
I therefore agree with the explicit prohibition of the approval of these active substances and EU citizens ought to be confident that none of these hazardous substances are present in the objects of everyday use and especially in drinking water, food or materials and objects intended for contact with food.
I also welcome the fact that it is possible to harmonise at a European level, but the costs for small and medium-sized enterprises should not be ruinous.
- (SK) Madam President, in relation to the recommendation of Parliament for the second reading of the regulation on the placing on the market and use of biocidal products, the Council and Parliament have adjusted their common position, where the Council has taken on board a large part of the proposals of Parliament, but only in a modified form.
For the sake of consistency and clarity, it was necessary to remove a number of inconsistencies contained in the text amending the proposals.
The position of the rapporteur, Christa Klaß, who states that to achieve the declared objectives it is necessary to eliminate the shortcomings of Directive 98/8/EC on authorisations and to simplify the entire decision-making process, is legitimate, and should, I think, be collectively supported.
The warning of the rapporteur about the duplication of regulations concerning labelling with other legal acts indicates that, in our legislative work, we should monitor more closely the various links and related legal measures.
The safety of handling biocidal products, especially in the environment of their professional users, is also a serious related issue, which we will also have to monitor very closely.
Member of the Commission. - Madam President, even if it is difficult to catch the eye, I will try to do my part.
I would like to thank everybody for their support.
Many Members made quite substantial comments with which I fully agree, and I will not repeat them.
On the question of sustainable use: for the record, we are in favour but we still need to work on the review of active substances used in biocides.
We will have a lot of work to do to implement this regulation - delegated acts, guidance documents and so on.
Therefore, let us focus firstly on the regulation and look at sustainable use on the basis of the report provided for in Article 17a.
Secondly, a short comment on confidentiality - an issue addressed many times in relation to this proposal over the past two-and-a-half years.
In my opinion, we have the right balance in the proposal between transparency and the legitimate protection of commercial interests.
Transparency was already a central principle of the Commission's initial proposal and, on the other hand, changes were introduced into the text limiting the amount of information which should be made public.
Finally, with regard to small and medium-sized enterprises, we in the Commission are very sensitive to the needs of SMEs, and the regulation does include a number of provisions that will help smaller companies: the possibility of waiving information data requirements thereby reducing costs; compulsory sharing of data; the provision on tests on vertebrate animals; a simplified and much less burdensome authorisation procedure for biocidal products based on safer ingredients, and so on.
Finally, I would again like to thank the rapporteur, Christa Klaß, for her excellent work and also for her cooperation with us, and I would certainly like to thank all of you who have stayed till the end of the evening with us.
The debate is closed.
The vote will take place at 12:00 noon on Thursday, 19 January 2012.
Written statements (Rule 149)
The aim of the new European biocide regulations is to exercise tighter control over the impact which they have on health and the environment, while also simplifying the procedure for authorising the products.
'Biocides' are products designed to fight harmful organisms and pathogenic germs.
They are used to protect human and animal health.
They include disinfectants and insecticides and are used for preserving materials, embalming or painting ships to prevent fouling from algae and micro-organisms.
The EU has fairly old legislation in this area, which has been in force since 1998.
Updating the legislation is a natural step to ensuring that only safe products access the market.
The new regulations cover new areas such as furniture and textiles.
The most hazardous substances, either carcinogenic or toxic for reproduction, should be banned or used with greater care.
The measures also provide a simpler authorisation procedure for placing products on the European market.
As far as animal testing is concerned, applicants must share studies on vertebrates in exchange for fair compensation.
In the future, an authorisation system needs to be introduced at EU level to establish a harmonised European biocides market.
One-minute speeches on matters of political importance (continuation)
Mr President, the eCall system in motor vehicles is a major advance in assisting emergency services to get to the scene of an accident as quickly as possible.
The device identifies the exact location of the accident so that the medical, police and fire services are able to get to the scene as quickly as possible.
This is particularly important in rural and isolated areas and particularly in the case of single car accidents.
eCall is now standard equipment in all new cars in many EU countries.
Unfortunately, it is not available in my own country, Ireland, where we still have an unacceptably high level of fatal accidents.
I believe that the system should be mandatory in all Member States.
This is about saving lives, and this device certainly saves lives.
That is the reason I am calling on the Commission to insist that all Member States insist that this device be available in all new motor vehicles.
