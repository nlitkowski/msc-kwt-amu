You are quite right to be insistent about the safety of these buses.
If you do not mind, we will ask the London authorities about this problem.
It is true that articulated buses can be easier to drive than rigid buses, but once the driver has weighed up what the vehicle can do, he tends to drive a bit faster and have more confidence when turning, sometimes a bit too much unfortunately.
Therefore, you raised a good point and we will try, as I have just mentioned, to enquire whether in fact the existing legislation needs to be amended.
Having said that, you were also right to raise this in view of the fact I underlined earlier, before this Assembly, that practically two thirds of accidents occur in towns and cities, and that, therefore, all the causes of these accidents must be understood.
Your highlighting of one of them has been timely.
Subject: Connecting Bucharest, Constanza and Sofia to the European high-speed rail network
One of the trans-European transport network's 30 priority projects is priority axis No 22, which includes the following railway sections: Athens-Sofia-Budapest-Vienna-Prague-Nuremberg/Dresden.
Romania is included in this project with the 480 km railway segment from Curtici to Brasov.
Given that since the accession of Romania and Bulgaria the European Union has gained an outlet to the Black Sea and has increased its population by some 30 million inhabitants, I should like to ask the Commission what steps Romania and Bulgaria have to take to connect the cities of Bucharest and Constanza, in the case of Romania, and Sofia, in the case of Bulgaria, to the European high-speed rail network for passengers and goods?
Mrs Ţicău, rail priority axis No 22 must, in effect, link Germany with Greece via Prague, Vienna, Budapest and Sofia.
A branch to the north of this axis will go, via Curtici and Brasov, to Bucharest and Constanza.
Romania and Bulgaria, as well as the other countries concerned, are making considerable efforts to construct this railway line so that eventually their respective capitals and the port of Constanza will be linked to the rail network of the rest of the European Union.
These efforts should continue during the period 2007-2013.
These projects are potentially eligible for Community funding through the Cohesion Fund or the Structural Funds, or even via the trans-European networks budget.
More specifically, Romania is prioritising the development of the Curtici-Brasov-Bucharest-Constanza railway line.
A 92 km section to the north of Bucharest has already been built.
Projects are under way between Bucharest and Constanza.
These should be progressively completed between now and 2010.
Romania has indicated, in its transport operational programme for 2007-2013, that it plans to develop the Curtici-Brasov-Predeal line using EUR 1.1 billion from the Cohesion Fund.
In terms of the Sofia link with the EU network, a major step was taken with the signing of the contract for the construction of the road and rail bridge over the Danube at Vidin-Calafat, financed by the ISPA Fund.
The bridge should be completed in 2010.
In addition, Bulgaria plans, in its transport operational programme for 2007-2013, to develop part of the Vidin-Sofia line.
The cost of the work to be carried out up to 2013 comes to EUR 320 million, out of a total of EUR 1 380 million for the entire line.
Romania is committed to completing, by 2013, all preparatory studies for the upgrading of the Craïova-Calafat line to the north of the Danube bridge, so that it is ready to carry out the work shortly after 2013.
There you have it.
I apologise for giving you all this information verbally, but obviously we can supplement this if necessary in writing, Mrs Ţicău, if you request it.
(RO) Mr. Commissioner, indeed, if possible, I would like to receive this information in writing as well.
I would also kindly ask you to organize a seminar on structural funds in Romania, because I consider that the new Member States need to be supported in order to be able to gain access to structural funds and develop their transport infrastructure.
The European Union's access to the Black Sea is important and, for this reason, I believe that, for the mobility of passengers and goods, it is also important to develop the railway.
I come back to the question regarding the high-speed train: the mobility of passengers is important and, for this reason, I request a dialogue in order to be able to achieve, step by step, whatever is necessary for this important project.
Mrs Ţicău, I am just as concerned as you are.
I would really like the new Member States, particularly Romania and Bulgaria in this case, to make the best use of the Cohesion Fund.
Certainly we must prioritise rail transport wherever possible, which will then help us avoid major problems later on in terms of environmental requirements and the risks of congestion on European motorways.
Thank you for your question.
I will ask my colleagues to answer more fully.
Once I have the chance to visit these countries, we will of course be able to provide you with even more information.
Subject: High-speed rail links Paris-Budapest and Strasbourg-Brussels
What is the Commission's opinion, following the basic agreements on the Stuttgart 21 project, of the timetable for the construction of the individual sections of the 'Magistrale for Europe' from Paris via Strasbourg and Munich to Vienna/Bratislava or Budapest?
What possibilities does the Commission see for expanding this Magistrale by a high-speed rail link between the three 'capitals of Europe' Strasbourg, Luxembourg and Brussels?
I will answer Mr Posselt.
The Paris-Strasbourg-Stuttgart-Vienna-Bratislava railway axis No 17 is one of the 30 priority projects of the trans-European transport network.
The Stuttgart 21 project that you mentioned is a major link in this priority project.
On 19 July 2007, an agreement was signed between Mr Tiefensee, the German Minister for Transport, and the authorities in the Federal State of Bade-Wurtemberg, the region and city of Stuttgart and Deutsche Bahn.
This agreement is a key phase in the elimination of the bottleneck represented by the Stuttgart-Ulm section, since it includes funding for the work to start in 2010.
The Commission is keeping a close eye on the modernisation of the sections between Stuttgart and Ulm.
In July 2005 it appointed a European coordinator for this line, Mr Péter Balázs, who has already submitted his second report.
This report was sent to the Chairman of Parliament's Committee on Transport and Tourism.
It is available on the website of the Directorate-General for Energy and Transport.
The coordinator, Mr Balázs, is continuing to give great attention to the Stuttgart-Ulm bottleneck, which must be eliminated.
In his report, the coordinator analysed the progress of the Paris-Bratislava priority project and concluded that most of the project could be completed in 2015.
However, given the complexity of the Stuttgart-Ulm section, this section will not be built until 2019.
It is clear that a priority project does not exclude continuation to other destinations.
Other priority projects, such as the Athens-Sofia-Budapest-Vienna-Prague-Nuremberg-Dresden axis, will extend the Paris-Bratislava project as far as the Black Sea and Greece, while the Eurocaprail priority No 28 project will link Brussels to Strasbourg via Luxembourg.
In July 2007 Member States submitted their proposals for Community cofinancing.
These proposals are currently being evaluated.
Based on this evaluation, the Commission will propose the RTE-T budget allocation for the period 2007-2013 to the European Parliament and Council.
Those are the answers I wanted to give you, Mr Posselt.
Personally I am extremely interested in the Paris-Budapest axis, and I can tell you that Mr Balázs and I are really keen for every effort to be made to complete this major project.
(DE) Commissioner, the historical credit for setting in motion what Mr Kohl and Mr Mitterrand had decided to do back in 1982, and which is now finally becoming reality, goes to you.
I have two brief questions.
Firstly, the Strasbourg-Kehl-Appenweier and the Munich-Mühldorf-Salzburg cross-border projects are also highly complex.
Is there any news on these?
Secondly: can you perhaps appeal once more to the rail companies - when we are already deploying so many European funds for cross-border routes - and ask them to stop cutting back cross-border train connections?
We are expanding the railways with our money at the same time as the rail companies are hacking transport routing to pieces with national timetables.
Naturally, I am quite ready to intervene.
Could you write to me on the subject, so that I can contact Deutsche Bahn?
I will also look into how we might improve things at this stage, since this is also part of our job: not only to plan these major corridors of the future, but to improve the situation now.
I will look forward to receiving a short memo from you that we can act on.
Subject: Final judgment by the Greek Court on Olympic Airways (OA)
In the discussion of my oral question of 13 March 2007 on 'the court decision offsetting debts of the Greek State to Olympic Airways (OA)', Commissioner Jacques Barrot said: 'The decision of the courts is a new aspect that we are analysing currently'.
And recent reports in the Greek press (3 June 2007) state that Commissioner Barrot, in his contacts with senior officials in the Greek Ministry of Economy and Finance during his most recent visit to Greece, 'allowed some margin for offsetting debts provided that progress was made with privatisation of OA'.
What exactly is the Commission analysing?
Is it considering whether the final judgment of the court of arbitration is in line with Community law?
Can the Commission confirm that it is negotiating on acceptance of the court decision provided that the Greek authorities go ahead with privatisation of OA?
Mr Papadimoulis asked me a question, which I will now answer.
The Commission has taken note of the Greek court of arbitration's decision of 13 July 2006 on flights carried out as a public service requirement, and of the decision of 6 December 2006 concerning the relocation of Olympic Airways to Athens International Airport.
The Greek authorities have already informally consulted the Commission on these decisions on compensation, which were the subject of a preliminary examination.
Based on that preliminary examination and the documents submitted at this stage by the Greek authorities, the Commission is still unable to adopt a position on the nature of these decisions under EU rules on State aid.
If these decisions were to involve further aid, they would obviously have to be notified under Article 88(3) of the Treaty.
The Commission's services informed the Greek authorities of this preliminary evaluation and we are awaiting their response.
Broadly speaking, the Commission is doing everything in its power to ensure that the aid which it has ordered to be recovered are actually recovered.
That is the information I can give you, Mr Papadimoulis.
(EL) Madam President, Commissioner, it is not just Olympic Airways that owes money to the state.
The state also owes money to Olympic Airways, a very large amount.
In total, Olympic Airways is demanding more than EUR 1 billion from the Greek state.
I would like to ask you whether the Commission intends to wait for, and especially to respect, the final judicial decisions by the Greek courts having jurisdiction.
I am informed that next week you are to meet with the responsible Greek Minister with whom you will be discussing these matters.
Can you outline for us which points you agree on and where you disagree with the Greek Government on this subject?
In Greece there is considerable concern, and a significant section of public opinion blames the Commission for liquidating Olympic Airways while it was still in operation, for a token price, for the benefit of its competitors.
What reply can you give on this subject?
For the time being, we are awaiting clarification from the Greek authorities on the ruling of the court of arbitration.
We have not been in contact recently with the Greek authorities concerning any privatisation plans.
The issue is not whether the court of arbitration has taken the correct decision but whether the contractual relations on which it was based as a point of departure are compatible with Community law.
I cannot say much more about it, Mr Papadimoulis, since, as you yourself stated, I have just agreed to a meeting with the new Greek minister.
I think that we will have the opportunity to discuss this with him then.
That is all I can tell you at the moment.
We need more information from the Greek authorities before we can take the necessary decisions.
Thank you, Commissioner, and thank you for dealing with that last question.
Questions which have not been answered for lack of time will receive written answers (see Annex).
That concludes Question Time.
(DE) Madam President, please allow me to speak on the Rules of Procedure.
I have been talking with my colleague, Mrs Madeira, and have also, of course, listened to the Commissioner's answers.
We have been asking our questions now for about three years and have not managed to get an oral answer to a single question.
Of course the written answer is also very detailed and correct, but it is easier to ask and make inquiries verbally and it makes the result even more interesting still.
Do you have any advice or guidance as to how this could perhaps be achieved in future?
Mr Leichtfried, thank you for your comments.
All I would say, in connection with the Working Party on the Internal Reform of Parliament, is that conduct of Question Time is one of the issues that we are looking at very carefully, and your comments will be taken on board.
(The sitting was suspended at 19.30 and resumed at 21.00)
Lapsed written statements: see Minutes
Approval of the Minutes
The Minutes of yesterday's session have been distributed.
Are there any comments?
Madam President, I rise under Rule 108 to make a personal statement because of the attack on myself and Mr Coates by the President-in-Office when he replied to the debate yesterday.
On page 28 of the Rainbow he suggests that we do not have any democratic mandate or legitimacy because we were elected on a Labour ticket and therefore we should not be sitting as Members of the House.
This is a serious personal attack and under Rule 108 of the Rules of Procedure I am allowed to reply.
I would observe that I was elected under a Labour ticket - not a New Labour ticket - when John Smith and Margaret Beckett were leaders of the party, and elected by two-thirds of my party members locally, unlike the Labour MEPs for next year who will be selected by Mr Blair personally.
So, I would claim to have a far stronger democratic mandate than they will have.
Madam President, I would like to praise the exemplary activity of the French security service during the visit by Tony Blair.
Secondly, I would like to inform you that President Gil-Robles has received a letter from Prime Minister Dehaene promising us that a police station will be established in the vicinity of the Parliament in Brussels.
Madam President, it concerns what has been lacking in the Minutes throughout the week, namely there has been no reference to Mr Falconer telling us whether he is voting or not.
Has there been any rule change by the Bureau with regard to recorded votes?
Thank you very much, Mr Wynn, for following the matter so carefully.
Madam President, I thank my colleague, Mr Wynn, for raising this matter.
Obviously, I would wish to allay Members' concern.
I am glad to see that the Monday to Friday group are still with us.
We are here all week and we vote all week, unlike some Members who are only present part-time and by Thursday afternoon are on the move.
The answer is quite simple.
I wrote to the President asking him how many Members had been affected as a result of the rule changes.
He advised me that per part-session since February 120 Members have had their allowances adjusted as a result.
I attend every month from Monday to Friday so I participate for the whole week.
I am opposed to the principle of how the Bureau came to the decision without referring the matter to the House, but I am appalled at the lack of backbone of these 120 Members who have stayed silent on this issue and have raised no protest in this Chamber.
With that in mind, I have decided to allow them to deal with their own consciences.
I will continue to support my principles and I have no doubt I will argue for those principles within the Socialist Group.
I hope this allays the House's concern.
Thank you, Mr Falconer, for the explanation.
We will record that in the Minutes.
(The Minutes were approved)
Votes
The amendment of Regulation No 1866/86 laying down certain technical measures for the conservation of fishery resources in the waters of the Baltic Sea is based on the latest recommendations adopted in the framework of the International Baltic Sea Fisheries Commission (IBSFC).
What are these technical measures?
They are merely minor adjustments to already existing prohibition periods for the fishing of flounder and plaice, and minor adjustments to prohibitions on the use of certain gear in fishing for salmon.
As the rapporteur has said, who, may I remind you, is Danish, ' These adjustments, and thus the Commission proposal, can easily be approved' .
However, I should like to remind you that wild salmon is in the process of disappearing from the Baltic Sea, a sea which receives special treatment from the Commission.
As we have seen in the Kindermann report, intensive industrial fishing is carried out in the Baltic.
Industrial fisheries use small-meshed nets and, despite protests made by Greenpeace, such activities are not subject to any particular restrictions.
The use of large-scale drift gillnets is still authorized, although this is an infringement of international legislation, which imposes a 2.5 km limit.
On the other hand, at present, Spanish vessels rarely fish in the Baltic.
Does the latter explain the former?
Our group supports this report in substance, as well as the proposal for a regulation, but wishes to stress that the Commission must remain impartial with regard to the fishermen of the various Member States.
We are therefore very firmly opposed to the fact that the Commission 'forgot' industrial fisheries when it established new technical measures for fisheries in Europe and that it 'forgot' to ask the users of large-scale drift gillnets to comply with international legislation.
It is inadmissible that the Commission should ignore the scientific considerations which form the basis of the Common Fisheries Policy, should favour some European fishermen undeservedly and should discriminate outrageously against others, in order to gain a large enough majority in the Council.
Kindermann report (A4-0201/98)
My group wishes to congratulate Mr Kindermann on the quality of his report on industrial fisheries.
As you know, industrial fisheries use small-meshed nets, the catches from which are intended for industrial processing into fish meal and fish oil.
Denmark is the largest producer of fish meal in the European Union, accounting for 80 % of industrial production and 6 % of the world market.
Fish meal is used mainly as animal feed in the poultry and pig-rearing sectors.
There is a remarkable disparity between the declared objective of the conservation of stocks and the huge quantities of fish caught for industrial purposes.
Our rapporteur quotes a number of particularly impressive statistics relating to industrial fisheries: 30 % of all world catches comes from industrial fisheries.
The proportion may be as high as 60 % in some seas, such as the North Sea, which is extremely vulnerable, as we all know.
By-catches are also large: sometimes they even exceed actual catch quotas.
There is an even more startling contrast between the Commission's uncompromising attitude towards certain types of fishery and its leniency towards industrial fisheries.
Does this distinction have anything to do with the fact that Spanish vessels are rarely to be found in northern waters?
Such discriminatory treatment by the Commission is not acceptable under any circumstances.
On the one hand, the use of drift gillnets has been prohibited without scientific basis in the Atlantic, where small catches are taken from very large stocks.
On the other hand, a good deal of research has been carried out, but no action has been taken with regard to industrial fisheries, which makes huge demands on resources.
What is the reason for this differential treatment?
If industrial fisheries constituted a significant activity in the Atlantic, they would most certainly be treated differently.
McCartin report (A4-0204/98)
As I said in my speech, our group tabled 15 amendments to Mr McCartin's report.
In fact, in contrast to the rapporteur's initial proposals, we believe it is necessary to have a strong COM for fishery products, particularly for products intended for the fresh fish market.
The approach adopted by the rapporteur is too exclusively liberal, going so far as to call explicitly for a reduction in the principle of Community preference.
I do, of course, know that the European Union has signed, and has made Member States sign, a number of free trade agreements.
It has made Member States join the generalized system of preferences, particularly with drug-producing countries.
We are also restricted by the GATT agreements within the framework of the WTO because, as a result of a major political error, unlike agricultural products, fishery products have always been consolidated under the GATT.
A COM which strengthens the entire fisheries sector is therefore essential, because of these major constraints, which already significantly limit Community preference.
We have a number of other possibilities, such as the improved organization of the sector, which would enhance the fresh fish market, and the continued use of market interventions.
But what does the rapporteur propose?
In terms of enhancement of the market, very little.
Instead of maintaining withdrawal systems, he proposes to do away with them.
What would be left of the COM?
One can but wonder.
The COM must not operate to the sole advantage of processors, whose main requirement is regularity of supply at the lowest cost, irrespective of the source of raw materials.
It is therefore essential for the WTO to establish specific measures applicable to the fresh fish market.
Varela Suanzes-Carpegna report (A4-0137/98)
Our group supports the overall approach adopted by Mr Varela in his report.
It is clearly essential to preserve, and therefore if necessary to protect, the fish canning industry and aquaculture in the European Union.
This is why my group tabled 16 amendments to the McCartin report on the COM for fisheries products.
The fisheries sector must be analyzed as a whole: it needs the processing sector but at the same time, and the Commission must accept this, if production were to cease in Europe, there would no longer be a European fish canning sector.
We therefore only support those of the rapporteur's proposals which aim to strengthen the canning industries and to stop relocations to countries where wages are low.
In his report, Mr Varela focuses on two important sectors of the canning industry: the sardine sector and the tuna sector.
The sardine sector, which has been virtually allowed to disappear in France, is still very important in the Iberian Peninsula.
The rapporteur proposes protecting this industry from imports of processed products from North African countries, particularly Morocco.
Although I am aware of the competition faced by the industry in Portugal and Spain, I would however like to point out that these North African countries have fragile economies which require support, unlike a certain number of other countries - especially in Latin America - with whom the European Union has signed preferential agreements to import tinned fish products, in particular within the framework of the so-called 'generalized system of preferences' .
With regard to the tuna industry, the rapporteur considers it necessary 'to ensure there is a proper supply of the necessary raw material (fresh, frozen and fillets of tuna), giving priority to the Community fleet?' .
The Spanish, French, Irish and British tuna fishing fleets in the Atlantic have contributed towards that supply until now.
I note that having worked actively in favour of the prohibition of fishing for tuna by European vessels with drift gillnets, Mr Varela is now asking for specific measures to protect the Community tuna processing industry: is it also destined, like fish stocks, to become a Spanish monopoly?
Finally, Mr President, I should like to remind the House that the fish product canning industry is highly diversified and not limited to the above two sectors of activity.
Furthermore, fish product processors, like all manufacturers, require regular supplies of raw materials at the lowest possible cost.
We must therefore very carefully ensure that the future of the processing sector is safe, but at the same time we must avoid doing so to the detriment of the fresh fish sector.
