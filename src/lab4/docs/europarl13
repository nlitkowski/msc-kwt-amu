For my part, I have no problem in obtaining tickets.
Really no problem at all.
There are an enormous number of sponsors ready to invite me!
But the ordinary citizen has a big problem!
(Applause) So on this basis we should admit the truth and admit that it is also our responsibility to try to improve the situation.
Mr President, this really is a very sorry tale, though I want to thank you and the Commissioner for at least allowing us to have that statement before we had the debate.
At least we get some idea of the full picture.
I certainly note the Commissioner's words very carefully, namely that this system is not fair and has been discriminatory.
And what has the Commission done?
It sent a warning apparently back in December, but we are now in March.
It has allowed three months to elapse while the situation has developed, and apparently we are now in a position where very little can be done about it.
I am sorry to have to say to the Commissioner that I do not think the Commission can entirely wash its hands of responsibility for the way this situation has developed.
It is interesting to know that in future we hope it is going to be avoided.
We have to ask the Commission this afternoon precisely what legal action and legal powers the Commission has to take against the organizers so that they face some penalty rather than simply expressions of regret?
The Commission has responsibility to exercise those powers which it has quite clearly and lamentably failed to exercise up to now.
The world championship is of interest to people all over the world and certainly throughout Europe, and the number of seats that has been made available to European football fans is derisory.
The suggestion that to get tickets they have to give an address in France makes one seriously wonder who was awake when that proposal was going through.
If we hear now that other national associations are expressing concern, I ask: were they given the full facts at the time?
That needs to be asked if they are now at this stage expressing their concern.
The Commission cannot simply come to us this afternoon an say it is a sorry tale but it is taking no further action.
It must take some action.
Mr President, I wish to say to the Commissioner that Liberals in this House are deeply disappointed with his statement today.
This is the first billion-dollar World Cup.
The organizers have put national restrictions on ticket sales in breach of Article 85 of the Treaties.
They are abusing a dominant market position in breach of Article 86.
The Commission was warned of this in June of last year in a letter from Professor Weatherill at Oxford University.
I am told that the Commission then had discussions with the CFO, who may have been less than honest with the Commission.
Very few football fans from outside France will get tickets to the matches.
Most of those who do will pay vastly inflated prices.
Liberals in this House call on the Commission, as guardian of the Treaties, to show the organizers the red card.
To those who say you cannot scrap an entire ticket system four months before the World Cup, we say: if it is illegal, it must be scrapped.
If the Commission decides there are good reasons to let it proceed, it must, at least, prosecute and fine the CFO.
Finding new arrangements to redistribute just 5 % will not do.
It is a sop to fans, stitched up with the CFO, and a cosy compact in changing rooms.
We are calling foul, and if we are not allowed to go to the stadia, then we may go to the courts.
Mr President, Commissioner, I would like to start by thanking you for being here today during this debate, for allowing us to be the first to receive information from you.
The lesson, the conclusion we can draw from what you have said is that the football associations and the organizers of the World Cup in France are playing with you, and are playing with us, but above all with all these people in Europe, and that only one conclusion can be drawn from this: we must act.
You, Commissioner, as the person responsible on behalf of Europe, have often compared your role with that of a referee, who has to take decisions, often in difficult areas, and who is therefore, depending on the circumstances, sympathetic or unsympathetic.
Well, we are keeping you to your word, and there is one thing you could do here, namely to show the red card to those breaking the European rules, to those who think they are above the law, to those who act with the greatest contempt toward European matters.
But we can say that of course we will follow the rules for the 100 000 remaining tickets, when more than 2.4 million have already been sold.
That would be satisfying us with crumbs, and it is simply not acceptable.
Secondly, we could say we are going to impose fines.
It would not be all that bad if Mr Platini or the other sun kings in France were momentarily affected financially, because in the end - as you said yourself - this is primarily a question of money, and a lot of money.
Let them pay those fines, but the drawback is of course that this does not benefit the ordinary citizen, and so we, Members of Parliament, should take a look at what is in our powers, what means we have to enforce Community law.
I would also like to address the French Members.
Let us be fair, our friends from France, and let us keep it clean, this is not a game against France.
This is not Germany, Belgium, the Netherlands, the United Kingdom or Italy against France.
This is a question of elementary respect for European rules.
If we raise this question today then it is not without consequences for the future.
What is the future, Commissioner, ladies and gentlemen?
The future is, amongst other things, the European Nations' Cup in 2000, organized by Belgium and the Netherlands.
For that, too, rules will have to be complied with, even if that is not very nice for the Dutch or Belgian football supporters.
Today it might not seem very nice to the French, but if we are not resolute, if we are not consistent, it will be worse for the Belgians or the Dutch tomorrow.
What is most important, however, is that we do not allow discrimination.
I would like to agree with Mr Watson; if we are not allowed into the grounds, we should go to Court.
I can tell you, Commissioner, that we, a number of Members of this Parliament, have already taken on a renowned lawyer.
Mr President, sometimes we in the European Parliament are rather flattered if people come to us asking for assistance and actually take us seriously.
So let me share with you a heartfelt letter which I received from a football fan from Aberdeen, in my constituency.
He says, ' I write to you in the hope that the European Parliament can do something about the ridiculous ticket allocations for the World Cup finals in France.
I have been a member of the Scotland Travel Club for eight years.
I have previously been to two World Cups and two European competitions yet I do not expect to be allocated tickets for the games.'
This man was hoping that, because the World Cup was in Europe, surely to goodness he would be able to go to the first match.
As you know, the opening match is Scotland versus Brazil and you would have thought that the people who above all would be there would be the Scots and the Brazilians.
Instead he finds that he is not wanted.
The allocation to Scotland is too small and he is not going to be there.
He may of course decide to go anyway and try to buy tickets on the black market.
I think this is a very serious situation and one which should have been avoided at all costs.
I take note of what the Commissioner said about the French authorities following the same procedures as in the past.
If the procedures have been wrong in the past that is no excuse for not reforming them.
I welcome the small steps that have been taken to alleviate the situation.
We are talking about something like 160 000 tickets which sounds like quite a lot but in fact is a very small number indeed.
I would make a plea to give priority to fans who are going to see their own team playing.
Obviously if France is playing then it is reasonable to expect them to have the lion's share of the tickets.
But when you have two small countries, Morocco and Scotland for example, then surely it would be quite easy to work out a mechanism to allow their fans to participate.
If there is one thing that makes a game enjoyable it is listening to the fans on both sides cheering their teams on.
I think it is time some common sense was brought into this whole process.
Mr President, there have certainly been delays in the Commission's initiative on this problem.
Nevertheless, I want to give my full support to the action being taken by the Commission and the Commissioner at this stage, because they are actually trying to remedy an unacceptable situation.
I am saying that even though I actually believe that the number of tickets that can be distributed is too small.
It is obviously a very low number compared with the total available.
The Commission is taking the right action and I think it is right and important that the Parliament should take a stance because it is becoming apparent that, behind all the major sports events and shows, lie economic interests and often real abuse, with the sole object of making huge profits, even by speculating.
This is the case we have before us, because it is unacceptable that you need to buy a whole package, including travel, accommodation and tourist services, just to be able to get a ticket.
Although we believe that the major sports events and shows, as the Commissioner said, should involve the European citizens and, I would add, young Europeans in particular, it is obvious that this discrimination is unacceptable.
From this point of view, I believe that, even on the basis of the position assumed by Parliament today, we need to assign the Commissioner a new mandate so that he will not only continue the work he has performed so far, but he will establish new bases for tickets, from the quantitative point of view, and start on the rules.
I appreciate the fact that the Commissioner has been very explicit in this connection: we need to change the FIFA rules that have, until today, obliged the authorities, such as the Commissioner and now Parliament, to intervene on these issues.
Mr President, we all agree on the problems: the shortage of seats owing to the objective limits of supply and demand; the excessive cost of tickets on indirect sale assigned to tour operators; the need to oppose the black market.
We are divided in our replies.
Can we consider the sale of tickets for a sports or cultural event to be an exclusively economic activity?
On reading the text of the draft arrangement, this would appear to be the predominant opinion, with reference to the rules laid down by the Treaty on competition, Articles 85 and 86.
Within this framework, however, there is the risk of moving from an alleged abuse of the national dominant position to an abuse of the dominant position held by the EU, since it is by definition an international event: the football World Cup.
However, having to draw up the accounts with the legitimate expectations of supporters worldwide, the fairest and most effective position seems to us to be that of ensuring the allocation of quotas assigned to the national federations and to the non-profit-making youth sports associations, in addition to direct sales.
Our amendments should be made to this effect; the outcome will determine our final vote.
Mr President, I should like first of all to thank the Commissioner for the efforts he has made.
I thought we were getting close to some kind of solution following a meeting I had with the Commissioner yesterday.
It would appear that solution has now disappeared and that the French authorities are now longer prepared to offer extra tickets in compensation to those people who have been disadvantaged by what is a clear breach of European Community law, namely Articles 85 and 86.
What worries me is the failure to state that the Commission is going to pursue this urgently and with vigour.
We cannot allow these people to get away with it.
There are tickets available.
Anybody round here can pick up a ticket for a World Cup match just like that.
All you have to do is pay about £600 a time through one of those all-inclusive travel packages, exactly the thing we were trying to stop.
There are 2.5 million tickets available.
I do not believe that the French authorities have sold the 22.3 % of those tickets available for direct sales.
I would like to have some figures from them.
As somebody else said, all that seems to be on offer at the moment is the crumbs.
This is a cynical commercial organization - not the people of France and not the French football authorities - that is trying to exploit the special nature of sport in the interests of making money and at the expense of the ordinary people of Europe.
I hope we are going to take this seriously and, as a number of speakers have said including Mr Watson and Mr Cunha, if we do not get far more than is offered at the moment from the Commission or the authorities this issue is likely to go to the Court, and we have to make sure that the Court judges, and quickly.
The World Cup is now less than 85 days away.
Madam President, it is clear that the Commission has lacked vigilance in this matter.
Three years ago, FIFA set out the rules.
We all knew what they were.
You should have intervened then.
Nor should 500 000 places have been offered to the sponsors to distribute through competitions and quizzes relating to whether you possess a certain brand of mineral water or tin of peas or not.
We all knew this.
Nothing was done.
Now it is right under our noses; the game is all but lost.
The 50 000 tickets that you mentioned, Commissioner, these are a drop in the ocean.
There are 166 federations, which is the equivalent of only around ten tickets per federation.
You have not even managed to obtain the other 100 000.
This is very serious.
In any case, in conclusion I would ask you first of all to do everything you can to prevent a black market from forming.
For the French will sell their tickets for matches such as the Belgium/Netherlands game at twenty times the price at which they bought them, which is unacceptable.
I would also ask you to play the game through to the end.
You can still intervene.
You can still try to scrape together places.
Indeed, you can still also use all your power to intervene on a legal level as well as on the level of compensation.
Madam President, we believe the concern over having to ensure the sacred principles of free competition and having to ensure the compatibility of initiatives with the rules of the internal market is the main reason for the spiralling increase in prices and offers in relation to the sale of tickets for the forthcoming 1998 World Cup to be held in France.
The Commission has found it necessary to disprove the accusations, according to which competition policy was at the origin of the spiralling increase in the prices of entrance tickets.
It sent a letter to FIFA contesting the astronomic prices reported by some press agencies and emphasizing that these increases relate not only to entrance to the stadiums but also to the price of the means of transport and accommodation expenses.
In our opinion, however, irrespective of the real or merely alleged price increases, the multiplication, times 15, of the prices applied by the travel agencies, the only agencies authorized to sell the World Cup package, are the direct consequence of the persistence of the Commission which is opposed to the establishment of a single retailer in each Member State.
Ensuring the principles of free competition, together with the fact of being able to overcome the accusation of abuse of a dominant position, should not be an obstacle to granting authorization to a single retailer in each Member State, specifically to prevent the emergence of the black market and, in particular, to prevent the monopolization of sales by the tourist agencies, thus giving all Community citizens the possibility of enjoying a sports event that is becoming increasingly more popular.
Madam President, the Commissioner has half-heartedly tackled FIFA on this matter.
What use is a slide tackle from Commissioner van Miert when you are dealing with people like Havelange and Lennart Johansen?
The same thing happened with freedom of movement: the Commission went very half-heartedly on this issue.
They really have to strengthen their tackling - they really must get to grips with the situation.
We are as concerned as the Commission for the Community citizen.
We are particularly concerned, and I am particularly concerned for the members of the Tartan army who are hoping to come from Scotland to see Scotland play Brazil and the ordinary fan is not going to be given the opportunity at all.
This week two tickets for the World Cup sold for £19 000 in Glasgow.
That is almost more than some of the ordinary people earn in about two years.
What opportunity will there be for ordinary people if these inflated prices are charged by whoever is organizing this World Cup?
As other members have said, it is up to the Commission to take strong action.
Madam President, the cultural, the social and indeed the commercial importance of sport is, I believe, beyond dispute.
The way that a football world championship binds peoples together has a quite special significance.
People retain memories of what happened there for the rest of their lives.
I therefore find it completely incomprehensible that the organizing committee for such an event uses a discriminatory ticket distribution system that gives preference to the citizens of its own country.
As the Commissioner has already remarked, the European Parliament is currently dealing with the second report on citizenship of the Union and is trying to find ways and means of making people feel European.
At the same time, sport, which already leads a rather modest existence in relation to its importance within the European Union, is being done a great disservice and a wedge is being driven between citizens of the Union.
In short, I can only regret that, if I have understood the Commissioner correctly, there is no possibility of straightening out the situation pro praeterito .
However, I must agree with the previous speakers and ask that we be informed of what is planned to prevent such things happening in future.
Madam President, I think I should first remind people that in relation to the rules of competition, the Commission is required to follow procedural rules.
When the problem first came to light in December, my departments immediately initiated a procedure, contacting the "accused' by means of letters.
There were also the reactions of some Members of Parliament.
But it was just some Members of Parliament, I think there were only two, although you now claim that you have been pleading with us since December to do something.
In any case, we acted promptly, as I said, by contacting the "accused' to ask them to explain the situation and to tell us if what we had heard was correct or not.
As the explanations we received were not satisfactory - by this time it was January - we decided to move rapidly on to the second phase of the procedure and to send a letter of warning to the interested parties.
Such speed in the sending of a letter of warning is extremely rare.
Those who normally criticize the Commission for moving too fast and sending a letter of warning without due consideration are now criticizing us for acting too slowly.
I am sorry but we did what we had to do, rapidly and with respect for the rules of the game.
Furthermore, the accused himself has rights.
This is my position and I stand by it.
You must realize that some things have been said today which I find unacceptable.
Secondly, as you know, we have had serious problems with sports organizations for quite some time now.
When I say "we' , I mean the Commission in particular.
In fact, I think I now clearly understand that when these organizations consider the Commission is getting in their way they merely say to themselves, "we'll go and find some support amongst the politicians' .
This is what happened in Germany.
Following a Court judgement, the political world rallied round to exempt sport from the rules of competition, saying to us, "It is no longer your business, do not get involved in this.
You, Mr Van Miert, and you, the authorities responsible for competition at national level, you deserve a red card, go and sit on the touchline.
We are going to deal with this.'
This is the reality of the situation.
So please, keep things in perspective.
On many occasions, I myself, on behalf of the Commission, have reminded everyone that the rules of competition must apply, including with regard to FIFA's behaviour.
Then, one month later, you probably read that a FIFA representative declared, in Singapore, if I remember correctly, that he was going to contact several European Prime Ministers in order to resolve the problem.
In other words, they were going to bypass the Commission for they were tiresome, these Commission officials who dealt with such problems.
This is the reality, can you believe it?
I met with more than one delegation of Members of Parliament who came to plead the cause of the sporting authorities.
I read yesterday in the papers that a minister - also in charge of sport - was going to come and see President Santer, my colleague Pádraig Flynn and myself to tell us that the Commission was no longer respecting the Bosman ruling of the Court.
This is the real situation.
But the Commission is holding firm, it is holding its course and it expects to make progress.
By the way, this is all apparently true for Formula 1 racing too.
So I repeat, I find some criticisms unacceptable.
On the other hand, I request your support in this matter.
When we have to tell FIFA that it must not only discuss with the Commission - which it generally refuses to do - but also change its system, can I count on the broad support of the European Parliament?
(Applause) Thank you very much, I appreciate that, but can I also count on your support in my discussions with certain national sports authorities?
(Applause) I am taking note of this.
When it comes to tackling the black market, Mr Monfils, please be assured that we will do all we can!
We will do all we can but, all the same, there is one thing that you cannot ask of the Commission and that is to take responsibility for organizing the World Cup.
This really is not our role.
Our role, on the basis of the rules of competition, is to check that offences are not committed, and to act if we consider they are.
As you know, this is my way and I think this is now understood, not only within Parliament but elsewhere too.
There is one other thing.
People have said, "what about the travel agencies?'
I have not forgotten them.
There is a problem here, too, for travel agencies must pay fees, they have to pay a great deal to be able to sell tickets.
But this still accounts for no more than 5.4 % of the tickets.
In principle, national organizations can also be contacted.
In principle, and here, too, there is a problem, for buyers should have been able to contact the organizers who should have made tickets available to them indiscriminately.
It is on this point that our action is centred.
As I said earlier, we have still not come to an agreement because, for the moment at least, we are not satisfied with the proposals made by the organizers to date.
We also want to check certain details which have been provided to us.
In fact, it seems that some of the categories I have mentioned are unclear and we would like to know precisely what they cover.
This is what we are going to do.
